package hpe3par.base;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import hpe3par.base.StorageSystem;
import utilities.ObjectConverter;

public class TestSystem {

	public static void main(String[] args) throws IOException {
		
		Path fileName = Path.of("C:\\abhishek\\workspace-spring-tool-suite-4-4.7.1.RELEASE\\3par-connector\\src\\test\\resources\\system.txt");
		//Path fileName = Path.of("C:\\Users\\ADanej\\Documents\\My Stuff\\workspace_sts4\\3par-connector\\src\\test\\resources\\system.txt");
		String json = Files.readString(fileName);

		System.out.println(json);
		
		StorageSystem system = (StorageSystem) ObjectConverter.getInstance().getObject2(json, StorageSystem.class);
		System.out.println(system.getName());
				
	}

}
