package hpe3par.base;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import api.Hpe3parAPIAccessor;

class VolumeCollectionTest {

	@Test
	void test() throws Exception {
		
		Hpe3parAPIAccessor api = new Hpe3parAPIAccessor();
		
		VolumeCollection vCol = api.getVolumes();
		
		System.out.println(vCol.getVolumeList().get(0).toString());
		// state 1 = normal
		System.out.println("State: " + Volume.State.toString(vCol.getVolumeList().get(0).getState()));
		
		System.out.println("apswesxvol.0: " + vCol.getByName("apswesxvol.0").toString());

		assertEquals(vCol.getTotal(), 10);
	}

}
