package hpe3par.base;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import api.Hpe3parAPIAccessor;
import hpe3par.base.PortCollection;
import hpe3par.base.StorageSystem;
import hpe3par.base.Port.PortMode;

class PortCollectionTest {


	@Test
	void test() throws Exception {
		

		Hpe3parAPIAccessor api = new Hpe3parAPIAccessor();
		
		PortCollection pCol = api.getPorts();
		System.out.println("PortCol: " + pCol.toString());
		System.out.println("port count: " + pCol.getTotal());

		
		System.out.println("mode: " + PortMode.toString(pCol.getPortList().get(0).getMode()));
		assertEquals(10, pCol.getTotal());
		assertEquals("TARGET",PortMode.toString(pCol.getPortList().get(0).getMode()));
		
	}

}
