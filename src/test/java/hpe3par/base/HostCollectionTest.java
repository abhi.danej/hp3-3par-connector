package hpe3par.base;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import api.Hpe3parAPIAccessor;
import hpe3par.base.HostCollection;
import hpe3par.base.Port.PortMode;

class HostCollectionTest {

	@Test
	void test() throws Exception {
		
		Hpe3parAPIAccessor api = new Hpe3parAPIAccessor();
				
		HostCollection hostCol = api.getHosts();
		System.out.println("Host at index 12: " + hostCol.getHostList().get(12).toString());
		System.out.println("host count: " + hostCol.getTotal());

		String name = hostCol.getHostList().get(12).getName();
		System.out.println("Host at Index 12 name: " + name);
		assertEquals(13, hostCol.getTotal());
		assertEquals("apswesx17",name);
	}

}
