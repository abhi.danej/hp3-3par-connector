package hpe3par.base;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import api.Hpe3parAPIAccessor;

class VolumesetsCollectionTest {

	@Test
	void test() throws Exception {
		
		Hpe3parAPIAccessor api = new Hpe3parAPIAccessor();
		
		VolumesetsCollection volumesetCol = api.getVolumesets();
		
		System.out.println(volumesetCol.getVolumesetList().toString());
		assertEquals(volumesetCol.getVolumesetList().get(0).getName(), "apswesxvol");
	}

}
