package hpe3par.base;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.Test;

class HostsetsCollectionTest {

	@Test
	void test() {
		Hostset hs1 = new Hostset();
		hs1.setName("apsw");
		hs1.setId(22);
		
		Hostset hs2 = new Hostset();
		hs2.setName("seasw");
		hs2.setId(44);
		
		HostsetsCollection hostsetCollection = new HostsetsCollection();
		hostsetCollection.setHostsetList(new ArrayList<Hostset>(Arrays.asList(hs1,hs2)));
		
		assertEquals(22, hostsetCollection.getByName("apsw").getId());;
		assertEquals(1, hostsetCollection.getByName("nonono").getId());
	}

}
