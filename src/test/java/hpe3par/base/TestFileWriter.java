package hpe3par.base;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import utilities.FileWriter;

class TestFileWriter {


	@Test
	void testAppend() throws IOException {
		
		FileWriter.getInstance().append("hello");
		assertEquals(1, 1);
	}

}
