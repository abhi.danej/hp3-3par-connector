package main;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.jupiter.api.Test;

import api.Hpe3parAPIAccessor;
import hpe3par.base.Node;
import hpe3par.base.StorageSystem;
import opcx.base.Instance;
import opcx.base.NewInstance;
import opcx.base.NewRelationship;
import opcx.base.Parent;
import opcx.base.Relations;
import opcx.base.Service;
import ucmdb.base.NodeNI;
import ucmdb.base.StorageSystemNI;

class TopoXMLGeneratorTest {
	
	Hpe3parAPIAccessor api = null;
	TopoXMLGenerator xmlGen = null;
	
	void setup() throws Exception {
		api = new Hpe3parAPIAccessor();
		xmlGen = new TopoXMLGenerator(null );
	}
	
	
	String getXML(ArrayList<NewInstance> instanceList, 
			ArrayList<NewRelationship> newRelationshipList) throws JAXBException, UnsupportedEncodingException {
		
		Service svc = new Service();
		
	    JAXBContext jc = JAXBContext.newInstance(Service.class);

		/*
		 * List<Attribute> aList = new ArrayList(); Attribute a1 = new Attribute();
		 * a1.setName("ucmdb_name"); a1.setValue("business_application"); aList.add(a1);
		 * 
		 * Attribute a2 = new Attribute(); a2.setName("ucmdb_monitored_by");
		 * a2.setValue("BSMC:Opscx"); aList.add(a2);
		 * 
		 * NewInstance n1 = new NewInstance(); n1.setKey("3par 8200");
		 * n1.setRef("3par"); n1.setStd("DiscoveredElement"); n1.setAttribute(aList);
		 * 
		 * NewRelationship rel1 = new NewRelationship(); Instance instance = new
		 * Instance(); instance.setRef("VM:myUniq1"); Instance instanceChild = new
		 * Instance(); instanceChild.setRef("SQL:mysql");
		 * 
		 * Parent p = new Parent(); p.setInstance(instance);
		 * 
		 * rel1.setParent(p);
		 * 
		 * Relations relation1 = new Relations(); relation1.setInstance(instanceChild);
		 * 
		 * 
		 * rel1.setRelationsList(new ArrayList<Relations>());
		 * rel1.getRelationsList().add(relation1);
		 * 
		 * svc.getNewRelationshipList().add(rel1);
		 * System.out.println("size of svc newinst array: " +
		 * svc.getNewInstance().size());
		 */
	    
	    svc.setNewInstanceList(instanceList);
	    svc.setNewRelationshipList(newRelationshipList);
	    
	    Marshaller marshaller = jc.createMarshaller();
	    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    marshaller.marshal(svc, baos);
	    String xml = null;
	    xml = new String(baos.toByteArray());
		return xml;
	}
	@Test
	void testGetStorageSystemInstance() throws Exception {
		
		setup();
		
		ArrayList<NewInstance> instList = new ArrayList<>();
		TopoXMLGenerator xmlGen = new TopoXMLGenerator(null );
		
		StorageSystem s = api.getStorageSystem();
		NewInstance instance = xmlGen.getNewInstance(s);
		instList.add(instance);
		
		ArrayList<Node> nodeList = api.getNodeList();
		for(Node n: nodeList) {
			instance = xmlGen.getNewInstance(n);
			instList.add(instance);
		}
		System.out.println("NewInstance size: " + instList.size());
		
		NewRelationship nRel = getStorageNodeRelationship(s, api.getNodeList());
		System.out.println(nRel.toString());
		ArrayList<NewRelationship> nrList = new ArrayList<>();
		nrList.add(nRel);
		
		String xml = getXML(instList, nrList);
		System.out.println(xml);
		
	}


	private NewRelationship getStorageNodeRelationship(StorageSystem s, ArrayList<Node> controllerNodeList) {
		System.out.println("Inside getStorageNodeRelationship");
		String rel_type = "containment";
		NewRelationship nRel = new NewRelationship();
		
		Parent parent = getParent(new StorageSystemNI(s).getRef());
		nRel.setParent(parent);
//		System.out.println("Parent set: " + parent.toString());
		ArrayList<Relations> rList = new ArrayList<>();
		for(Node n: controllerNodeList) {
//			System.out.println("Starting for: " + n.getName());
			Relations r= getRelations(rel_type, new NodeNI(n).getRef());
			rList.add(r);
		}
		
		nRel.setRelationsList(rList);
		
		return nRel;
	}
	
	private Relations getRelations(String type, String ref) {
		Relations r = new Relations();
		r.setInstance(getInstance(ref ));
		r.setType(type);
		return r;
	}
	
	private Instance getInstance(String ref) {
//		System.out.println("insdie getInstance with ref " + ref);
		Instance i = new Instance();
		i.setRef(ref);
		return i;
	}


	private Parent getParent(String ref) {
//		System.out.println("Inside getParent for ref " + ref);
		Parent p = new Parent();
		p.setInstance(getInstance(ref));
		return p;
	}

}
