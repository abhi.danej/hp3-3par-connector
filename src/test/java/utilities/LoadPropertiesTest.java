package utilities;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.junit.jupiter.api.Test;

class LoadPropertiesTest {

	@Test
	void test() throws FileNotFoundException, IOException {
		Properties p = LoadProperties.getInstance().props;
		System.out.println(p.getProperty("3par.host"));
		System.out.println(p.getProperty("3par.port"));
		System.out.println(p.getProperty("3par.username"));
		System.out.println(p.getProperty("3par.password"));
		assertEquals("15.114.163.253", p.getProperty("3par.host"));
	}

}
