package utilities;

import static org.junit.jupiter.api.Assertions.*;

import javax.xml.bind.JAXBException;
import org.junit.jupiter.api.Test;

class PojoToXmlConverterTest2 {

	@Test
	void test() throws JAXBException {

        PojoToXmlConverter2 p2x = new PojoToXmlConverter2();
        p2x.convert();
        assertEquals(1, 1);
	}

}
