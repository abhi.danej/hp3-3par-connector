package utilities;

import static org.junit.jupiter.api.Assertions.*;

import javax.xml.bind.JAXBException;
import org.junit.jupiter.api.Test;

class PojoToXmlConverterTest {

	@Test
	void test() throws JAXBException {

        PojoToXmlConverter p2x = new PojoToXmlConverter();
        p2x.convert();
        assertEquals(1, 1);
	}

}
