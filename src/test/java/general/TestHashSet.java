package general;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.google.common.collect.Sets;

import hpe3par.base.Host;
import hpe3par.base.Volume;

class TestHashSet {

	@Test
	void test() {
		var hostSet = new HashSet<>();
		
		var h1 = new Host();
		h1.setId(1);
		h1.setName("AbhishekHost");
		
		var h2 = new Host();
		h1.setId(2);
		h1.setName("AbhishekHost2");
		
		hostSet.add(h1);
		
		Volume v1 = new Volume();
		v1.setId(1);
		v1.setName("volume");
		var vSet = new HashSet<>();
		vSet.add(v1);
		
		
		Set<Host> set1 = Sets.newHashSet();
		set1.add(h1);
		
		assertEquals(1, set1.size());
	}

}
