package api;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SessionKeyManagerTest {

	@Test
	void test() throws Exception {
		
		SessionKeyManager skm = new SessionKeyManager();
		
		String key = skm.getKey( new APIHttpClient().getClient(), "https://10.0.0.30:8080/api/v1/", "3paradm", "3pardata");
		
		System.out.println(key);
		
		assertEquals(1, 1);
		
	}

}
