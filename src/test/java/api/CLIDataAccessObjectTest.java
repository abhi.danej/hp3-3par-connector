package api;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import hpe3par.base.DriveEnclosure;
import hpe3par.base.PhysicalDrive;

class CLIDataAccessObjectTest {

	@Test
	void testGetDriveEnclosuresList() throws FileNotFoundException, IOException {

		CLIDataAccessObject dao = new CLIDataAccessObject();

		ArrayList<DriveEnclosure> enclosureList = dao.getDriveEnclosuresList();
		System.out.println(enclosureList);
		
		ArrayList<PhysicalDrive> driveList = dao.getPhysicalDrivesList();
		System.out.println(driveList);

		assertEquals(0, driveList.get(0).getDriveEnclosureId());

	}
}
