package api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CLIParser {

	public static void main(String[] args) throws IOException {

		String showpdFile = "sampledata\\showpd.txt";
		String showcageFile = "sampledata\\showcage.txt";
		
//		List<String> fileLines = Files.readAllLines(Path.of(showpdFile));
//		fileLines.forEach(line -> {
//			System.out.println(line);
//		});
		
		/*        ----Size(MB)---- ----Ports----
		Id CagePos Type RPM State     Total    Free A      B      Capacity(GB)
		0 0:0:0   FC    10 normal   559104   55296 0:1:1* 1:1:1           600
		*/ 
		System.out.println("Physical Drive");
		String content = Files.readString(Path.of(showpdFile));
		String patternString = "[^0-9:]([0-9]{1,2})"
				+ "\\s+"
				+ "([0-9]+):[0-9]+:[0-9]+"
				+ "\\s+"
				+ "([a-zA-Z]+)"
				+ "\\s+"
				+ "([0-9]+)"
				+ "\\s+"
				+ "([a-zA-Z]+)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher m = pattern.matcher(content);
		while(m.find()) {
			String driveId = m.group(1);
			String enclosureId = m.group(2);
			String type = m.group(3);
			String rpm = m.group(4);
			String state = m.group(5);
			System.out.println(
					"drive:" + driveId 
					+ ", state: " + state
					+ ", rpm: " + rpm
					+ ", type: " + type
					+ ", enclosure: " + enclosureId);
			
		}
		
		System.out.println("CAGE");
		content = Files.readString(Path.of(showcageFile));
		String patternString_old = "[^0-9:]([0-9]{1,2})"
		+ "\\s+"
		+ "(.*)"
		+ "\\s+"
		+ "([0-9]+:[0-9]+:[0-9]+)"
		+ "\\s+"
		+ "\\d+"
		+ "\\s+"
		+ "([0-9]+:[0-9]+:[0-9]+)"
		+ ".*"
		+ "\\s+"
		+ "([A-Za-z0-9]+)"
		+ "\\s+"
		+ "[A-Za-z0-9]+\r";
		patternString = "[^0-9:]([0-9]{1,2})"
			+ "\\s+"
			+ "(.*)"
			+ "\\s+"
			+ "([0-9]+:[0-9]+:[0-9]+)"
			+ "\\s+"
			+ "\\d+"
			+ "\\s+"
			+ "([0-9]+:[0-9]+:[0-9]+)"
			+ ".*"
			+ "\\s+"
			+ "([A-Za-z0-9]+)"
			+ "\\s+"
			+ "[A-Za-z0-9]+"
			+ "\\s+"
			+ "\\R";
		pattern = Pattern.compile(patternString);
		m = pattern.matcher(content);
		while(m.find()) {
//			String id = matcher.group(1);
//			String name = matcher.group(2);
//			String model = matcher.group(3);
//			System.out.println("cageId:" + id + ", cageName: " + name + ", model: " + model);
			
			String id = m.group(1);
			String name = m.group(2);
			String aPortString = m.group(3);
			String bPortString = m.group(4);
			String model = m.group(5);
			System.out.println("cageId: " + id + ", cageName: " + name + ", model: " + model);
		}
		
	}

}
