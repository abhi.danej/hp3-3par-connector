package hpe3par.base;

import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(exclude = {"loopAPort","loopBPort","physicalDriveList"})
@EqualsAndHashCode(callSuper = true)
@Data
public class DriveEnclosure extends PortDevice {

	public Integer id = -1;
	public String name = null;	
	public String aPortString = null;
	public String bPortString = null;
	public Port loopAPort;
	public Port loopBPort;
	private String deviceType="DriveEnclosure";
	public ArrayList<PhysicalDrive> physicalDriveList;
	// to do
	public String model = null;
}
