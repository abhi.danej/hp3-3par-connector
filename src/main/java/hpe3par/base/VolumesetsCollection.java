package hpe3par.base;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class VolumesetsCollection {
	private Integer total;
	
	@JsonProperty("members")
	private ArrayList<Volumeset> volumesetList;
	
	public Volumeset getByName(String name) {
		Volumeset volSet = null;
		 try {
			volSet = volumesetList.stream().filter(volumeset -> (volumeset.getName().equals(name))).findFirst().get();
		} catch(Exception e) {
			System.out.println("Volumeset with name: " + name + " not found.");
		} 
		System.out.println("Volumeset: " + volSet.toString());
		 
		return volSet;
	}
}
