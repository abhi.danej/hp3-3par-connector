package hpe3par.base;

import java.util.ArrayList;

import lombok.Data;
import lombok.ToString;

@ToString(exclude = {"storage","portList"})
@Data
public class Node {

	private String name;
	private Integer nodeId;
	private boolean isMaster = false;
	private boolean isInCluster = false;
	
	private ArrayList<Port> portList;
	private StorageSystem storage;
}


/*
	Name = 4C17121222-0 (id)

*/