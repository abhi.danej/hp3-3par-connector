package hpe3par.base;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@ToString(exclude = {"volumeList", "hostsetList", "hostList"})
@Data
public class Volumeset {

    private Integer id = -1;
    private String name = null;
    private String domain;
    private String comment;
    private boolean qosEnabled;
    
    @JsonProperty("setmembers")
    private ArrayList<String> volumeNameList;
    
    //calculated fields
    private ArrayList<Volume> volumeList;
    private StorageSystem system;
    private ArrayList<Hostset> hostsetList;
    private ArrayList<Host> hostList;
}
