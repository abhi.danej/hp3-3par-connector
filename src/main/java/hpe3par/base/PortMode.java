package hpe3par.base;

public enum PortMode implements WsapiEnumIF {
    UNKNOWN_TO_API(-1),
    SUSPENDED(1),
    TARGET(2),
    INITIATOR(3),
    PEER(4);

    private int value;

    /**
     * Primary constructor for this enumeration.
     * @param inValue The value corresponding to the enumerated object.
     */
    private PortMode(final int inValue) {
        value = inValue;
    }

    /**  {@inheritDoc} */
    public Integer integerValue() {
        return value;
    }

    /**
     * Returns String representation of the enumerated object
     * corresponding to the argument.
     * Returns "unrecognized" if argument does not correspond to any
     * value in this enueration.
     * @param i integer value of the enumerated object.
     * @return String object corresponding to argument.
     */
    public static String toString(final int i) {
        for (PortMode m: PortMode.values()) {
            if (i == m.value) {
                return m.name();
            }
        }
        return (new String(i + " (unrecognized)"));
    }

    /** {@inheritDoc} */
    public String toString() {
        return name();
    }
}