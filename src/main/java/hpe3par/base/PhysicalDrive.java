package hpe3par.base;

import lombok.Data;

@Data
public class PhysicalDrive {

	private Integer id = -1;
	private	Integer driveEnclosureId = -1; 
	private Integer rpm = -1;
	private String type;
	private String state;
	// post processing
	public DriveEnclosure driveEnclosure = null;
	// to-do Port connection

}
