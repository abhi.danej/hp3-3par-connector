package hpe3par.base;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@ToString(exclude = {"hostList","volumeList","volumeset"})
@Data
public class Hostset {

    private Integer id;
    private String name;
    private String domain;
    private String comment;
    
    @JsonProperty("setmembers")
    private ArrayList<String> hostNameList;
    private Integer flashCachePolicy;
    
    //calculated field
    private ArrayList<Host> hostList;
    private StorageSystem system; 
    private ArrayList<Volume> volumeList;
    private ArrayList<Volumeset> volumesetList;
}
