package hpe3par.base;

import lombok.Data;

@Data
public class HostFCpaths {
    private String wwn;
    private PortPos portPos;
    private String firmwareVersion;
    private String vendor;
    private String model;
    private String driverVersion;
    private Integer hostSpeed;
}