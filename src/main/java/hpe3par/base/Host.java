package hpe3par.base;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Sets;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(exclude = {"portList","hostset","volumeset","volumeList","volumeHashSet"})
@EqualsAndHashCode(callSuper = false)
@Data
public class Host extends PortDevice {

	@EqualsAndHashCode.Include
    private Integer id = -1;
	
	@EqualsAndHashCode.Include
    private String name = null;;
    private String domain;
    private HostDescriptors descriptors;
    private String deviceType="Host";
    
    @JsonProperty("FCPaths")
    private ArrayList<HostFCpaths> fcPathList;
    //later    private iSCSIpaths[] iSCSIPaths;
    private Integer persona;
    private Boolean initiatorChapEnabled;
    private String initiatorChapName;
    private Boolean targetChapEnabled;
    private String targetChapName;
    
    // calculated fields
    private ArrayList<Port> portList = null;
    private Hostset hostset;
    private ArrayList<Volumeset> volumesetList;
    private ArrayList<Volume> volumeList = null;
//    private Set<Volume> volumeHashSet = Sets.newHashSet() ;
    //= new HashSet<>();
    
    /**
     * Enumeration of the host persona.
     */
    public enum Persona implements WsapiEnumIF {

        UNKOWN_TO_API(-1),
        GENERIC(1),
        GENERIC_ALUA(2),
        GENERIC_LEGACY(3),
        HPUX_LEGACY(4),
        AIX_LEGACY(5),
        EGENERA(6),
        ONTAP_LEGACY(7),
        VMWARE(8),
        OPENVMS(9),
        HPUX(10),
        AIX(11);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private Persona(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        public void setValue(final int inValue) {
            value = inValue;
        }

        /**
         * Returns String object representing the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enumeration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (Persona p : Persona.values()) {
                if (i == p.value) {
                    return p.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of the host pathOperation.
     */
    public enum PathOperation implements WsapiEnumIF {

        UNKOWN_TO_API(-1),
        ADD(1),
        REMOVE(2);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private PathOperation(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        public void setValue(final int inValue) {
            value = inValue;
        }

        /**
         * Returns String object representing the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enumeration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (PathOperation p : PathOperation.values()) {
                if (i == p.value) {
                    return p.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of the host chapOperation.
     */
    public enum ChapOperation implements WsapiEnumIF {

        UNKOWN_TO_API(-1),
        ADD(1),
        REMOVE(2);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private ChapOperation(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        public void setValue(final int inValue) {
            value = inValue;
        }

        /**
         * Returns String object representing the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enumeration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (ChapOperation c : ChapOperation.values()) {
                if (i == c.value) {
                    return c.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of the host chapOperationMode.
     */
    public enum ChapOperationMode implements WsapiEnumIF {

        UNKOWN_TO_API(-1),
        INITIATOR(1),
        TARGET(2);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private ChapOperationMode(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        public void setValue(final int inValue) {
            value = inValue;
        }

        /**
         * Returns String object representing the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enumeration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (ChapOperationMode m : ChapOperationMode.values()) {
                if (i == m.value) {
                    return m.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }
}
