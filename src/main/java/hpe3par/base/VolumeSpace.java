package hpe3par.base;

import lombok.Data;

@Data
public class VolumeSpace {

    public Integer reservedMiB = -1;
    public Integer rawReservedMiB = -1;
    public Integer usedMiB = -1;
    public Integer freeMiB = -1;
}
