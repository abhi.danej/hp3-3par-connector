package hpe3par.base;

import java.util.ArrayList;

import lombok.Data;
import lombok.ToString;

@ToString(exclude = {"deviceList","node"})
@Data
public class Port {

    private PortPos portPos;
    private Integer mode;
    private Integer linkState;
    private String  nodeWWN;
    private String  portWWN;
    private Integer type;
    private Integer protocol;
    private String  label;
    private ArrayList<String>  device;
    private PortPos partnerPos;
    private Integer failoverState;
    private String  IPAddr;
    private String  iSCSIName;
    
    private String portId;
    private Node node;
    
    // host and driveenclosures
    private ArrayList<PortDevice> deviceList;
    
    public void postProcessing() {
    	setPortId(portPos.getNode() + ":" + portPos.getSlot() + ":" + portPos.getCardPort());
    }
    
    /**
     * Enumeration of port mode.
     * <code>Port.PortMode</code> objects provide information about mode of the
     * port.
     */
    public enum PortMode implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        SUSPENDED(1),
        TARGET(2),
        INITIATOR(3),
        PEER(4);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the enumerated object.
         */
        private PortMode(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns String representation of the enumerated object
         * corresponding to the argument.
         * Returns "unrecognized" if argument does not correspond to any
         * value in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (PortMode m: PortMode.values()) {
                if (i == m.value) {
                    return m.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of port link state
     * <code>Port.PortState</code> objects provide information about link
     * state of the port.
     */
    public enum PortState implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        CONFIG_WAIT(1),
        ALPA_WAIT(2),
        LOGIN_WAIT(3),
        READY(4),
        LOSS_SYNC(5),
        ERROR_STATE(6),
        XXX(7),
        NONPARTICIPATE(8),
        COREDUMP(9),
        OFFLINE(10),
        FWDEAD(11),
        IDLE_FOR_RESET(12),
        DHCP_IN_PROGRESS(13),
        PENDING_RESET(14);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private PortState(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns String representation of the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (PortState s: PortState.values()) {
                if (i == s.value) {
                    return s.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of port connection type.
     * <code>Port.PortType</code> objects provide information about
     * connection type of the port.
     */
    public enum PortType implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        HOST(1),
        DISK(2),
        FREE(3),
        IPORT(4),
        RCFC(5),
        PEER(6),
        RCIP(7),
        ISCSI(8),
        CNA(9);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private PortType(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns String representation of the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (PortType t: PortType.values()) {
                if (i == t.value) {
                    return t.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of port protocol.
     * <code>Port.Protocol</code> objects provide info about protocol type
     * of the port.
     */
    public enum Protocol implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        FC(1),
        ISCSI(2),
        FCOE(3),
        IP(4),
        SAS(5);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private Protocol(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns String representation of the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (Protocol p: Protocol.values()) {
                if (i == p.value) {
                    return p.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of port failover state.
     * <code>Port.FailoverState</code> objects provide information about
     * failover state of the port.
     *
     */
    public enum FailoverState implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        NONE(1),
        FAILOVER_PENDING(2),
        FAILED_OVER(3),
        ACTIVE(4),
        ACTIVE_DOWN(5),
        ACTIVE_FAILED(6),
        FAILBACK_PENDING(7);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private FailoverState(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns String representation of the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (FailoverState f: FailoverState.values()) {
                if (i == f.value) {
                    return f.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * <code>Port.Query</code> objects contain an array of
     * <code>Port.Properties</code> objects returned by the HPSD WSAPI.
     */
}