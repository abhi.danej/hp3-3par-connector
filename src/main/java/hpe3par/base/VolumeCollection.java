package hpe3par.base;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class VolumeCollection {
	private Integer total;
	
	@JsonProperty("members")
	private ArrayList<Volume> volumeList;
	
	public Volume getByName(String name) {
		Volume volume = null;
		try {
			volume = volumeList.stream().filter(vol -> (vol.getName().equals(name))).findFirst().get();
		} catch(Exception e) {
			System.out.println("Volume with name: " + name + " not found.");
			System.out.println(e.getMessage());
		} 
		
		return volume;
	}
}
