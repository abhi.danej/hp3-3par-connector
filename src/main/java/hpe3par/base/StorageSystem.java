package hpe3par.base;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@ToString(exclude = {"controllerNodeList"})
@Data
public class StorageSystem {
 
        private Integer id = -1;
        private String name = "3PAR STORAGE";
        private String systemVersion;
        private String patches;
        
        @JsonProperty("IPv4Addr")
        private String IPv4Addr = "some_ip";
        private String IPv6Addr;
        private String model;
        private String serialNumber;
        private Integer totalNodes;
        private Integer masterNode;
        private Integer[] onlineNodes;
        private Integer[] clusterNodes;
        private Integer chunkletSizeMiB;
        private Integer totalCapacityMiB;
        private Integer allocatedCapacityMiB;
        private Integer freeCapacityMiB;
        private Integer failedCapacityMiB;
        private String  location;
        private String  owner;
        private String contact;
        private String comment;
        private String  timeZone;
        
        private ArrayList<Node> controllerNodeList;

}

/*

  {
    "id": 120447,
    "name": "3PAR8200",
    "systemVersion": "3.2.2.612",
    "patches": "P50,P51,P54",
    "IPv4Addr": "10.0.0.30",
    "model": "HP_3PAR 8200",
    "serialNumber": "4C17121222",
    "totalNodes": 2,
    "masterNode": 0,
    "onlineNodes": [
        0,
        1
    ],
    "clusterNodes": [
        0,
        1
    ],
    "chunkletSizeMiB": 1024,
    "totalCapacityMiB": 26836992.0,
    "allocatedCapacityMiB": 24534016.0,
    "freeCapacityMiB": 2302976.0,
    "failedCapacityMiB": 0.0,
    "timeZone": "Asia/Singapore",
    "licenseInfo": {
        "issueTimeSec": 1490068598,
        "issueTime8601": "2017-03-21T11:56:38+08:00",
        "diskCount": 0,
        "WWNBASE": 120447,
        "licenses": [
            {
                "name": "3PAR OS Suite"
            },
            {
                "name": "Adaptive Flash Cache"
            },
            {
                "name": "Autonomic Rebalance"
            },
            {
                "name": "Peer Motion",
                "expiryTimeSec": 1521676800,
                "expiryTime8601": "2018-03-22T08:00:00+08:00"
            },
            {
                "name": "System Reporter"
            },
            {
                "name": "Thin Conversion"
            },
            {
                "name": "Thin Copy Reclamation"
            },
            {
                "name": "Thin Deduplication"
            },
            {
                "name": "Thin Persistence"
            },
            {
                "name": "Thin Provisioning (10240000G)"
            },
            {
                "name": "VSS Provider for Microsoft Windows"
            }
        ],
        "licenseState": {
            "virtualCopy": false,
            "remoteCopy": false,
            "thinProvisioing": true,
            "domains": false,
            "dynamicOptimization": false,
            "virtualLock": false,
            "thinPersistence": true,
            "thinConversion": true,
            "adaptiveOptimization": false,
            "peerVirtualization": true,
            "qos": false,
            "systemReporter": true,
            "darEncryption": false,
            "fileServices": false,
            "storageFederation": false,
            "onlineImport": false,
            "rmcApplicationSuite": false,
            "smartSAN": false
        }
    },
    "parameters": {
        "rawSpaceAlertFC": 0,
        "rawSpaceAlertNL": 0,
        "rawSpaceAlertSSD": 0,
        "remoteSyslog": false,
        "remoteSyslogHost": "0.0.0.0",
        "sparingAlgorithm": "Default",
        "eventLogsize": 3145728,
        "VVRetentionTimeMax": 1209600,
        "upgradeNote": "",
        "portFailoverEnabled": true,
        "autoExportAfterReboot": true,
        "allowR5OnNLDrives": false,
        "allowR0": false,
        "thermalShutdown": true,
        "failoverMatchedSet": true,
        "sessionTimeout": 3600,
        "hostDIF": true,
        "allowWrtbackSingleNode": false,
        "allowWrtbackUpgrade": false,
        "disableDedup": false
    }
}
 */
