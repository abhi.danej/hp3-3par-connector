package hpe3par.base;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class HostsetsCollection {
	private Integer total;
	
	@JsonProperty("members")
	private ArrayList<Hostset> hostsetList;
	
	public Hostset getByName(String name) {
		Hostset hs = null;
		 try {
			hs = hostsetList.stream().filter(hostset -> (hostset.getName().equals(name))).findFirst().get();
		} catch(Exception e) {
			System.out.println("Hostet with name: " + name + " not found.");
		} 
		 
		return hs;
	}
}
