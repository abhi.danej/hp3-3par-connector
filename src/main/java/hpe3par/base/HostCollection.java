package hpe3par.base;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class HostCollection {
	private Integer total;
	
	@JsonProperty("members")
	private ArrayList<Host> hostList;
	
	public Host getByName(String name) {
		Host host = null;
		 try {
			host = hostList.stream().filter(h -> (h.getName().equals(name))).findFirst().get();
		} catch(Exception e) {
			System.out.println("Host with name: " + name + " not found.");
		} 
		 
		return host;
	}
}
