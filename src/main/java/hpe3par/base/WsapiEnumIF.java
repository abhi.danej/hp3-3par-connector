package hpe3par.base;

/**
 * Interface implemented by HPSD WSAPI enumerations for consistency.
 */
public interface WsapiEnumIF {
	/**
	 * Method to return the <code>Integer</code> value associated with the
	 * enumerated object.
	 * 
	 * @return <code>Integer</code> corresponding to this enumerated object.
	 */
	Integer integerValue();
}
