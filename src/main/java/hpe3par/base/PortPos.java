package hpe3par.base;

import lombok.Data;

@Data
public class PortPos {
	
    private Integer node;
    private Integer slot;
    private Integer cardPort;
}