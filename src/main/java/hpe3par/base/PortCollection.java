package hpe3par.base;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PortCollection {
	private Integer total;
	
	@JsonProperty("members")
	private ArrayList<Port> portList;
}
