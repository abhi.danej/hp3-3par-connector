package hpe3par.base;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.google.common.collect.Sets;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString(exclude = {"volumesetList","hostset","hostList", "hostHashSet"})
@Data
public class Volume {

	@EqualsAndHashCode.Include
	public Integer id = -1;
	
	@EqualsAndHashCode.Include
    public String name = null;
    public String domain;
    public Integer provisioningType;
    public Integer copyType;
    public String copyOf;
    public Integer baseId;
    public Boolean readOnly;
    public Integer state;
    public Integer[] failedStates;
    public Integer[] degradedStates;
    public Integer[] additionalStates;
    public VolumeSpace adminSpace;
    public VolumeSpace snapshotSpace;
    public VolumeSpace userSpace;
    public Integer sizeMiB;
    public Integer parentId;
    public Integer roChildId;
    public Integer rwChildId;
    public Integer physParentId;
    public String wwn;
    public Integer creationTimeSec;
    public String creationTime8601;
    public Integer expirationTimeSec;
    public String expirationTime8601;
    public Integer retentionTimeSec;
    public String retentionTime8601;
    public VolumePolicies policies;
    public String userCPG;
    public String snapCPG;
    public String uuid;
    public String comment;
    public Integer usrSpcAllocWarningPct;
    public Integer usrSpcAllocLimitPct;
    public Integer ssSpcAllocWarningPct;
    public Integer ssSpcAllocLimitPct;
    
    // calculated
    public ArrayList<Volumeset> volumesetList;
    public ArrayList<Hostset> hostsetList;
    public ArrayList<Host> hostList;
//    public Set<Host> hostHashSet = Sets.newHashSet();
    //= new HashSet<>();
    
    /**
     * Enumeration of the virtual volume provisioning types.
     */
    public enum ProvType implements WsapiEnumIF {

        UNKOWN_TO_API(-1),
        UNSET(0),
        FULL(1),
        TPVV(2),
        SNP(3),
        PEER(4),
        UNKNOWN(5);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private ProvType(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns String object representing the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (ProvType p : ProvType.values()) {
                if (i == p.value) {
                    return p.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of the types of copy that the VV may be.
     */
    public enum CopyType implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        UNSET(0),
        BASE(1),
        PHYSICAL_COPY(2),
        VIRTUAL_COPY(3);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private CopyType(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns <code>String</code> representation of the enumerated
         * object corresponding to argument.  Returns "unrecognized" if the
         * argument does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (CopyType p : CopyType.values()) {
                if (i == p.value) {
                    return p.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of the basic (high-level) states that a VV may be in.
     */
    public enum State implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        UNSET(0),
        NORMAL(1),
        DEGRADED(2),
        FAILED(3);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private State(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns <code>String</code> representation of the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final int i) {
            for (State p : State.values()) {
                if (i == p.value) {
                    return p.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of the detailed states that a VV may be in.
     */
    public enum DetailedState implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        UNSET(0),
        LDS_NOT_STARTED(1),
        NOT_STARTED(2),
        NEEDS_CHECK(3),
        NEEDS_MAINT_CHECK(4),
        INTERNAL_CONSISTENCY_ERROR(5),
        SNAPDATA_INVALID(6),
        PRESERVED(7),
        STALE(8),
        COPY_FAILED(9),
        DEGRADED_AVAIL(10),
        DEGRADED_PERF(11),
        PROMOTING(12),
        COPY_TARGET(13),
        RESYNC_TARGET(14),
        TUNING(15),
        CLOSING(16),
        REMOVING(17),
        REMOVING_RETRY(18),
        CREATING(19),
        COPY_SOURCE(20),
        IMPORTING(21),
        CONVERTING(22),
        INVALID(23);

        private int value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The value corresponding to the
         * enumerated object.
         */
        private DetailedState(final int inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns String object representing the enumerated object
         * corresponding to argument.  Returns "unrecognized" if the argument
         * does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final Integer i) {
            for (DetailedState p : DetailedState.values()) {
                if (i.intValue() == p.value) {
                    return p.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

}
