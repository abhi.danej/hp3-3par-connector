package hpe3par.base;

import lombok.Data;

@Data
public class VolumePolicies {

    public Boolean staleSS;
    public Boolean oneHost;
    public Boolean zeroDetect;
    public Boolean system;
    public Boolean caching;
}
