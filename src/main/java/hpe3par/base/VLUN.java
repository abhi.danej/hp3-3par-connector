package hpe3par.base;

import lombok.Data;

@Data
public class VLUN {

    private int lun;
    private String volumeName;
    private String hostname;
    private String remoteName;
    private PortPos portPos;
    private Integer type;
    private String volumeWWN;
    private Integer multipathing;
    private Integer failedPathPol;
    private Integer failedPathInterval;
    private String hostDeviceName;
    private Boolean active;
    
    /**
     * Enumeration of VLUN Types.
     */
    public enum Type implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        UNSET(0),
        EMPTY(1),
        PORT(2),
        HOST(3),
        MATCHED_SET(4),
        HOST_SET(5);

        private Integer value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The <code>Integer</code> value corresponding to the
         * enumerated object.
         */
        private Type(final Integer inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns <code>String</code> representation of the
         * <code>VLUN.Type</code> object corresponding to the argument.
         * Returns "unrecognized" if argument does not correspond to any
         * value in this enueration.
         * @param i integer value of the enumerated object.
         * @return <code>String</code> object corresponding to argument.
         */
        public static String toString(final Integer i) {
            for (Type t: Type.values()) {
                if (i == t.value) {
                    return t.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }

    /**
     * Enumeration of multi-pathing types used by VLUN.
     */
    public enum MultiPathType implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        UNSET(0),
        UNKNOWN(1),
        ROUND_ROBIN(2),
        FAILOVER(3);

        private Integer value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The <code>Integer</code> value corresponding to the
         * enumerated object.
         */
        private MultiPathType(final Integer inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns <code>String</code> representation of the
         * <code>MultiPathType</code> corresponding to argument.  Returns
         * "unrecognized" if the argument
         * does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final Integer i) {
            for (MultiPathType t: MultiPathType.values()) {
                if (i == t.value) {
                    return t.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }

    }

    /**
     * Enumeration of multi-pathing types used by VLUN.
     */
    public enum FailedPathPolicy implements WsapiEnumIF {
        UNKNOWN_TO_API(-1),
        UNSET(0),
        UNKNOWN(1),
        SCSI_TEST_UNIT_READY(2),
        INQUIRY(3),
        READ_SECTOR0(4);

        private Integer value;

        /**
         * Primary constructor for this enumeration.
         * @param inValue The <code>Integer</code> value corresponding to the
         * enumerated object.
         */
        private FailedPathPolicy(final Integer inValue) {
            value = inValue;
        }

        /**  {@inheritDoc} */
        public Integer integerValue() {
            return value;
        }

        /**
         * Returns <code>String</code> representation of the
         * <code>VLUN.FailedPathPolicy</code> corresponding to argument.
         * Returns "unrecognized" if the argument
         * does not correspond to any object in this enueration.
         * @param i integer value of the enumerated object.
         * @return String object corresponding to argument.
         */
        public static String toString(final Integer i) {
            for (FailedPathPolicy t: FailedPathPolicy.values()) {
                if (i == t.value) {
                    return t.name();
                }
            }
            return (new String(i + " (unrecognized)"));
        }

        /** {@inheritDoc} */
        public String toString() {
            return name();
        }
    }
}
