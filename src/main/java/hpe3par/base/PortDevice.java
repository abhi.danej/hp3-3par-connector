package hpe3par.base;

import lombok.Data;

@Data
public class PortDevice {

	public Integer id = -1;
	public String name = null;
	public String deviceType = null;

	public PortDevice() {
		super();
	}

}