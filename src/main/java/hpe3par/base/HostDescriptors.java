package hpe3par.base;

import lombok.Data;

@Data
public class HostDescriptors {

    private String location;
    private String IPAddr;
    private String os;
    private String model;
    private String contact;
    private String comment;
}
