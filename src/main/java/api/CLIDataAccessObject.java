package api;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hpe3par.base.DriveEnclosure;
import hpe3par.base.PhysicalDrive;
import utilities.FileWriter;

public class CLIDataAccessObject {

	private Hpe3parSshAPIAccessor sshApi = null;
	private Map<String, String> cmdPatternMap = Map.of(
			"showpd","[^0-9:]([0-9]{1,2})"
					+ "\\s+"
					+ "([0-9]+):[0-9]+:[0-9]+"
					+ "\\s+"
					+ "([a-zA-Z]+)"
					+ "\\s+"
					+ "([0-9]+)"
					+ "\\s+"
					+ "([a-zA-Z]+)"	,
			"showcage", "[^0-9:]([0-9]{1,2})"
					+ "\\s+"
					+ "(.*)"
					+ "\\s+"
					+ "([0-9]+:[0-9]+:[0-9]+)"
					+ "\\s+"
					+ "\\d+"
					+ "\\s+"
					+ "([0-9]+:[0-9]+:[0-9]+)"
					+ ".*"
					+ "\\s+"
					+ "([A-Za-z0-9]+)"
					+ "\\s+"
					+ "[A-Za-z0-9]+"
					+ "\\s+"
					+ "\\R"
			);
	
	public CLIDataAccessObject() throws FileNotFoundException, IOException {
		sshApi = new Hpe3parSshAPIAccessor();
	}
	
	public ArrayList<PhysicalDrive> getPhysicalDrivesList() throws IOException {
		
		String cmd = "showpd";
		System.out.println("Executing getPhysicalDrives: " + cmd);
		
		ArrayList<PhysicalDrive> driveList = null;
		String content = sshApi.exec(cmd);
		appendToFile(cmd, content);
		Pattern p = Pattern.compile(cmdPatternMap.get(cmd));
		Matcher m = p.matcher(content);
		while(m.find()) {
			if(driveList==null) {
				driveList = new ArrayList<>();
			}
//			String id = m.group(1);
//			String driveEnclosureId = m.group(2);
//			String state = m.group(3);
			String id = m.group(1);
			String enclosureId = m.group(2);
			String type = m.group(3);
			String rpm = m.group(4);
			String state = m.group(5);
			System.out.println(
					"drive:" + id 
					+ ", state: " + state
					+ ", type: " + type
					+ ", rpm: " + rpm
					+ ", enclosure: " + enclosureId);
			
			PhysicalDrive drive = new PhysicalDrive();
			drive.setId(Integer.parseInt(id));
			drive.setDriveEnclosureId(Integer.parseInt(enclosureId));
			drive.setRpm(Integer.parseInt(rpm));
			drive.setType(type);
			drive.setState(state);
			driveList.add(drive);
		}
		
		return driveList;
	}
	
	private void appendToFile(String command, String content) throws IOException {
		FileWriter.getInstance().append("\nSSH " + command);
		FileWriter.getInstance().append(content);
	}


	public ArrayList<DriveEnclosure> getDriveEnclosuresList() throws IOException {
		String cmd = "showcage";
		System.out.println("Executing getDriveEnclosuresList: " + cmd);

		ArrayList<DriveEnclosure> deList = null; 

		String content = sshApi.exec(cmd);
		appendToFile(cmd, content);
		Pattern p= Pattern.compile(cmdPatternMap.get(cmd));
		Matcher m = p.matcher(content);
		while(m.find()) {
			if(deList==null) {
				deList = new ArrayList<>();
			}
			String id = m.group(1);
			String name = m.group(2);
			String aPortString = m.group(3);
			String bPortString = m.group(4);
			String model = m.group(5);
			System.out.println("cageId: " + id + ", cageName: " + name + ", model: " + model);
			DriveEnclosure enclosure = new DriveEnclosure();
			enclosure.setId(Integer.parseInt(id));
			enclosure.setName(name);
			enclosure.setModel(model);
			enclosure.setAPortString(aPortString);
			enclosure.setBPortString(bPortString);
			deList.add(enclosure);
		}
		
		return deList;
	}

}
