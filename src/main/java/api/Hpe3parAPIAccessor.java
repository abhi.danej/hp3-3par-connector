package api;

import java.io.FileNotFoundException;
import java.io.IOException;

import utilities.FileWriter;
import utilities.LoadProperties;
import utilities.ObjectConverter;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.URI;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import hpe3par.base.HostCollection;
import hpe3par.base.HostsetsCollection;
import hpe3par.base.Node;
import hpe3par.base.PhysicalDrive;
import hpe3par.base.PortCollection;
import hpe3par.base.StorageSystem;
import hpe3par.base.VLUNCollection;
import hpe3par.base.VolumeCollection;
import hpe3par.base.VolumesetsCollection;
import hpe3par.base.DriveEnclosure;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class Hpe3parAPIAccessor {

	protected String urlServer;
	protected String urlBase;
	protected String urlWsapi;
	protected String sessionKey = null;
	protected HttpClient httpClient;
//	protected FileWriter fw = FileWriter.getInstance();
	protected Hpe3parSshAPIAccessor sshApi;
	// for parsing Node data, as there is no separate query for node.
	protected StorageSystem storage = null;

	private HashMap<Class<?>, String> map = new HashMap<>();

	public Hpe3parAPIAccessor()
			throws Exception {
		
		Properties p = LoadProperties.getInstance().props;
		String storageHost = p.getProperty("3par.host");
		String port = p.getProperty("3par.port");
		String username = p.getProperty("3par.username");
		String password = p.getProperty("3par.password");

		urlServer = "https://" + storageHost + ":" + port;
		urlBase = urlServer + "/api";
		urlWsapi = urlBase + "/v1/";
		
		setClassApiMap();
		
		getHttpClient();
		
		getSessionKey(username, password);
		
	}

	private void setClassApiMap() {
		map.put(StorageSystem.class, "system");
		map.put(PortCollection.class, "ports");
		map.put(HostCollection.class, "hosts");
		map.put(HostsetsCollection.class, "hostsets");
		map.put(VolumeCollection.class, "volumes");
		map.put(VolumesetsCollection.class, "volumesets");
		map.put(VLUNCollection.class, "vluns");
	}

	private void getHttpClient() throws KeyManagementException, NoSuchAlgorithmException {		
		httpClient = new APIHttpClient().getClient();
	}

	public VLUNCollection getVLUNs() throws IOException, InterruptedException {
		return (VLUNCollection) doGet(VLUNCollection.class);
	}
	
	public StorageSystem getStorageSystem() throws IOException, InterruptedException {
		//return (StorageSystem) doGet("system");
		storage = (StorageSystem) doGet(StorageSystem.class);
		return storage;
	}
	
	public ArrayList<Node> getNodeList() {
		ArrayList<Node> nodeList = new ArrayList<>();
		for (int id : storage.getClusterNodes()) {
			Node n = new Node();
			n.setNodeId(id);
			n.setName(storage.getSerialNumber() + "-" + id);
			n.setInCluster(true);
			if (storage.getMasterNode() == id) {
				n.setMaster(true);
			}
			nodeList.add(n);
		}
		return nodeList;
	}
	
	public PortCollection getPorts() throws IOException, InterruptedException {
		return (PortCollection) doGet(PortCollection.class);
	}
	
	public HostCollection getHosts() throws IOException, InterruptedException {
		return (HostCollection) doGet(HostCollection.class);
	}
	
	public HostsetsCollection getHostsets() throws IOException, InterruptedException {
		return (HostsetsCollection) doGet(HostsetsCollection.class);
	}
	
	public VolumeCollection getVolumes() throws IOException, InterruptedException {
		return (VolumeCollection) doGet(VolumeCollection.class);
	}
	
	public VolumesetsCollection getVolumesets() throws IOException, InterruptedException {
		return (VolumesetsCollection) doGet(VolumesetsCollection.class);
	}
	
	public ArrayList<DriveEnclosure> getDriveEnclosuresList() throws FileNotFoundException, IOException {
		CLIDataAccessObject dao = new CLIDataAccessObject();
		return dao.getDriveEnclosuresList();
	}
	
	public ArrayList<PhysicalDrive> getPhysicalDriveList() throws FileNotFoundException, IOException {
		CLIDataAccessObject dao = new CLIDataAccessObject();
		return dao.getPhysicalDrivesList();
	}
	
	private Object doGet(Class outputClass) throws IOException, InterruptedException {
		
		HttpRequest request = HttpRequest.newBuilder()
				.GET()
				.uri(URI.create(urlWsapi + map.get(outputClass)))
				.setHeader("X-HP3PAR-WSAPI-SessionKey", sessionKey)
				.build();
		
		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
		
		if (response.statusCode()==200) {
			String json = response.body();
			appendToFile(outputClass, json);
			return parseJson(outputClass,json);
		} else {
			System.out.println(response.statusCode());
			System.out.println(response.body());
			return null;
		}
		
	}

	private void appendToFile(Class outputClass, String json) throws IOException {
		FileWriter.getInstance().append("\nGET /" + map.get(outputClass));
		FileWriter.getInstance().append(json);
	}

	private Object parseJson(Class outputClass, String json) throws JsonMappingException, JsonProcessingException {
		System.out.println("Response:\n" + json);
		@SuppressWarnings("unchecked")
		Object obj = ObjectConverter.getInstance().getObject2(json, outputClass);
		return obj;
	}

	private void getSessionKey(String user, String password) throws Exception {
		SessionKeyManager skm = new SessionKeyManager();
		sessionKey = skm.getKey(httpClient, urlWsapi, user, password);
//		sessionKey = "0-b5dd713f6eff814e9dcc8369bd054731-7ec92f5f";
	}

}