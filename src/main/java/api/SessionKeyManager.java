package api;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.net.http.HttpResponse;
import java.util.Date;
import java.util.Properties;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.time.Instant;

public class SessionKeyManager {
	
	private String sessionKey = null; 
	private long sessionKeyExpirationDate = 0;
	private Properties keyProps = new Properties();
	private String keyFile ="data\\keyfile.txt";
	
	
	public String getKey(HttpClient httpClient, String urlWsapi, String user, String password) throws Exception {
		if(isLocalKeyValid()) {
			return sessionKey;
		} else {
			sessionKey = getNewKey(httpClient, urlWsapi, user, password) ;
			Instant after24Hours = Instant.now().plusSeconds(86400L);
			sessionKeyExpirationDate = after24Hours.getEpochSecond();
			
			storeKeyLocally(); 				
		}
		
		return sessionKey;
	}

	public void storeKeyLocally() throws Exception {

		System.out.println("Trying to store access key details locally.");
		if( sessionKey != null
				&& sessionKey.length() != 0
				&& sessionKeyExpirationDate != 0
				) {			
			
			keyProps.setProperty("session_key", sessionKey);
			
			keyProps.setProperty("expiration_date", String.valueOf(sessionKeyExpirationDate));
			keyProps.store(new FileOutputStream(keyFile), " DO NOT MODIFY MANUALLY");
			System.out.println("Session key details stored.");
		} else {
			System.out.println("Cannot store value of access_key: " + sessionKey + ", expiration_date: " + sessionKeyExpirationDate);
		}
	}

	public boolean isLocalKeyValid() throws FileNotFoundException, IOException {
		if(Files.exists(Paths.get(keyFile))) {
			System.out.println("Found session key file.");
			keyProps.load(new FileReader(keyFile));		
			
			try {
				sessionKey = keyProps.getProperty("session_key");
				sessionKeyExpirationDate = Long.parseLong(keyProps.getProperty("expiration_date"));
			} catch(Exception e) {
				throw new RuntimeException("Cannot read access_key and expiration_date propterty from file at path");
			}
			Instant in = Instant.now();
			long epochSeconds = in.getEpochSecond();
			System.out.println("Current epoch time: " + epochSeconds + " | " + new Date());
			System.out.println("Local key Expiration Date value: " + 
					sessionKeyExpirationDate + " | " + new Date(sessionKeyExpirationDate*1000));
			
			// if it is expired.
			if(this.sessionKeyExpirationDate > epochSeconds) {
				System.out.println("Local key is valid.");
				return true;
			} else {
				System.out.println("Local key has expired.");
				return false;
			}
			
		} else {
			System.out.println("Did not find session key file.");
			return false;
		}
	}

	public String getNewKey(HttpClient httpClient, String urlWsapi, String user, String password) throws IOException, InterruptedException {
		System.out.println("Getting new Session Key.");
        String jsonBody = new StringBuilder()
                .append("{")
                .append("\"user\":\"")
                .append(user)
                .append("\",")
                .append("\"password\":\"")
                .append(password)
        		.append("\"")
                .append("}").toString();
        
        System.out.println("Body: " + jsonBody);
		
		HttpRequest request = HttpRequest.newBuilder()
					.POST(HttpRequest.BodyPublishers.ofString(jsonBody))
					.uri(URI.create(urlWsapi + "credentials"))
					.setHeader("Content-Type", "application/json")
					.build();
			
			HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
			
			if (response.statusCode()==201) {
				String json = response.body();
				System.out.println("Response: " + json );
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jNode = mapper.readTree(json);
				return jNode.get("key").asText();
//				return "ok";
			} else {
				return null;
			}
			
		}
	}

