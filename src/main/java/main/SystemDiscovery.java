package main;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import api.Hpe3parAPIAccessor;
import hpe3par.base.DriveEnclosure;
import hpe3par.base.Host;
import hpe3par.base.HostCollection;
import hpe3par.base.Hostset;
import hpe3par.base.HostsetsCollection;
import hpe3par.base.Node;
import hpe3par.base.PhysicalDrive;
import hpe3par.base.Port;
import hpe3par.base.StorageSystem;
import hpe3par.base.VLUN;
import hpe3par.base.Volume;
import hpe3par.base.VolumeCollection;
import hpe3par.base.Volumeset;
import hpe3par.base.VolumesetsCollection;
import utilities.TopoWriter;

public class SystemDiscovery {
	
	private StorageSystem storage;
	private ArrayList<Node> controllerNodeList;
	private ArrayList<Port> portList;
	private ArrayList<Host> hostList;
	private ArrayList<Hostset> hostsetList;
	private HostCollection hostCollection;
	private HostsetsCollection hostsetCollection;
	private ArrayList<Volume> volumeList;
	private VolumeCollection volumeCollection;
	private ArrayList<Volumeset> volumesetList;
	private VolumesetsCollection volumesetCollection;
	private ArrayList<PhysicalDrive> physicalDriveList;
	private ArrayList<DriveEnclosure> driveEnclosureList;
	private ArrayList<VLUN> vlunList;
	
	public static String configFilePath;
	
	public ArrayList<PhysicalDrive> getPhysicalDriveList() {
		return physicalDriveList;
	}

	public ArrayList<DriveEnclosure> getDriveEnclosureList() {
		return driveEnclosureList;
	}


	public ArrayList<Host> getHostList() {
		return hostList;
	}

	// TODO create a separate main class
	public static void main(String[] args) {
		
		
		try {
			
			if(args[0].equals("")) {
				System.out.println("java -jar app.jar conf/conf.properties");
			}
			
			configFilePath = args[0];

			SystemDiscovery disco = new SystemDiscovery();
			disco.go();
			
			new TopologyBuilder(disco).build();
			
			String xml = new TopoXMLGenerator(disco).create();
			TopoWriter.getInstance().append(xml);
		} catch (FileNotFoundException e) {
			System.out.println("Provide path to configuration file.");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Provide path to configuration file.");
			System.out.println("java -jar app.jar conf/conf.properties");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void go() throws Exception {
		
		Hpe3parAPIAccessor api = new Hpe3parAPIAccessor();
		
		storage = api.getStorageSystem();
		System.out.println("STORAGE:\n" + getStorage().toString());
		
//		controllerNodeList = inferNodes(getStorage());
		controllerNodeList = (ArrayList<Node>) api.getNodeList();
		System.out.println("\nController Nodes:\n" + getControllerNodeList().toString());
		
		portList = api.getPorts().getPortList();
		System.out.println("\nPort List:\n" + getPortList().toString());
		
		hostCollection = api.getHosts();
		hostList = hostCollection.getHostList();
		System.out.println("\nHost List Size:\n" + hostList.size());
		
		hostsetCollection = api.getHostsets(); 
		hostsetList = hostsetCollection.getHostsetList();
		System.out.println("\nHostset List:\n" + getHostsetList());
		
		volumeCollection = api.getVolumes();
		volumeList = volumeCollection.getVolumeList();
		System.out.println("\nVolume List:\n" + getVolumeList());
		System.out.println("\nVolume List Size: " + getVolumeList().size());
		
		volumesetCollection = api.getVolumesets(); 
		volumesetList = volumesetCollection.getVolumesetList();
		// when no volume set is created in Storage, create a default/placeholder set for consistency
		if(volumesetList.size()==0) {
			//createDefaultVolumeset();
		}
		System.out.println("\nVolumeset List:\n" + getVolumesetList());
		
		
		vlunList = api.getVLUNs().getVlunList();
		System.out.println("\nVLUN List size: " + vlunList.size());
		
		driveEnclosureList = api.getDriveEnclosuresList();
		System.out.println("\nDriveEnclosure List:\n" + getDriveEnclosureList());
		
		physicalDriveList = api.getPhysicalDriveList();
		System.out.println("\nPhysicalDrive List:\n" + getPhysicalDriveList());
		
		printSummary();
		
		System.out.println("SYSTEM DISCOVERY COMPLETE");
		
	}
	
	private void printSummary() {
		System.out.println("SUMMARY: ");
		System.out.println("Storage: " + getStorage().getName());
		System.out.println("Controller: " + getControllerNodeList().size());
		getControllerNodeList().forEach(node -> {
			System.out.println("\t" + node.getName());
		});
		System.out.println("Port: " + getPortList().size());
		getPortList().forEach(p -> {
			System.out.println("\t"   + p.getPortId());
		});
		System.out.println("Host: " + getHostList().size());
		getHostList().forEach(host -> {
			System.out.println("\t" + host.getName());
		});
		System.out.println("Hostset: " + getHostsetList().size());
		getHostsetList().forEach(hostset -> {
			System.out.println("\t" + hostset.getName());
		});
		System.out.println("Volume: " + getVolumeList().size());
		getVolumeList().forEach(x -> {
			System.out.println("\t" + x.getName());
		});
		System.out.println("Volumeset: " + getVolumesetList().size());
		getVolumesetList().forEach(x -> {
			System.out.println("\t" + x.getName());
		});
		System.out.println("DriveEnclosure: " + getDriveEnclosureList().size());
		getDriveEnclosureList().forEach(x -> {
			System.out.println("\t" + x.getName());
		});
		System.out.println("PhysicalDrive size: " + getPhysicalDriveList().size());
		System.out.println("VLUN size: " + getVlunList().size());
	}


	private void createDefaultVolumeset() {
		Volumeset volumeset = new Volumeset();
		volumeset.setId(0);
		volumeset.setName("default");
//		volumeset.setVolumeList(volumeList);
		volumeset.setVolumeNameList(new ArrayList<String>());
		
		getVolumeList().forEach(volume -> {
//			if(volume.getVolumesetList()==null) {
//				volume.setVolumesetList(new ArrayList<Volumeset>());
//			}
//			volume.getVolumesetList().add(volumeset);
			if(volumeset.getVolumeNameList()==null) {
			}
			volumeset.getVolumeNameList().add(volume.getName());
		});
		
		if(volumesetList.size()==0) {
			volumesetList = new ArrayList<Volumeset>(Arrays.asList(volumeset));
		}
		
	}

	public ArrayList<Volume> getVolumeList() {
		return volumeList;
	}

	public ArrayList<Volumeset> getVolumesetList() {
		return volumesetList;
	}

	public StorageSystem getStorage() {
		return storage;
	}

	public ArrayList<Node> getControllerNodeList() {
		return controllerNodeList;
	}

	public ArrayList<Port> getPortList() {
		return portList;
	}
	
	public ArrayList<Hostset> getHostsetList() {
		return hostsetList;
	}

	public ArrayList<VLUN> getVlunList() {
		return vlunList;
	}

	public HostsetsCollection getHostsetCollection() {
		return hostsetCollection;
	}

	public VolumesetsCollection getVolumesetCollection() {
		return volumesetCollection;
	}

	public VolumeCollection getVolumeCollection() {
		return volumeCollection;
	}

	public HostCollection getHostCollection() {
		return hostCollection;
	}

}
