package main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import hpe3par.base.Host;
import hpe3par.base.Hostset;
import hpe3par.base.Node;
import hpe3par.base.Port;
import hpe3par.base.PortDevice;
import hpe3par.base.PortPos;
import hpe3par.base.Volume;
import hpe3par.base.Volumeset;
import hpe3par.base.PhysicalDrive;

public class TopologyBuilder {
	
	private SystemDiscovery d = null;
	
	public TopologyBuilder(SystemDiscovery d) {
		this.d = d;
	}
	

	public void build() {

		/*
		 * System - Controller Node relationship
		 */
		joinStorageAndControllerNodes();
		
		/*
		 * Port id processing
		 */
		d.getPortList().forEach(port -> {
			port.setPortId(port.getPortPos().getNode() + ":" + port.getPortPos().getSlot() + ":" + port.getPortPos().getCardPort());
		});

		
		joinControllersAndPorts();
		
		/*
		 * Port - Host
		 * only port of Type = 1 connect to Hosts
		 */
		joinPortsAndHosts();
		
		/*
		 * Port - Host
		 * only port of Type = 2 connect to DriveEnclosure
		 */
		joinPortsAndDriveEnclosures();
		
		joinPhysicalDrivesAndDriveEnclosures();
		
		joinHostsetsAndStorage();
		
		joinHostsAndHostsets();
		try {
			joinVolumesetsAndVolumes();
		} catch (Exception e) {
			System.out.println("Failed in joinVolumesetsAndVolumes");
			e.printStackTrace();
		}
		
		//this method to be called AFTER all these entities are discovered;
		joinVolumesAndVolumesetsAndHostsAndHostsetsUsingVLUN();
		printVLUNConnections();
		
//		printSummary();

		System.out.println("\nEnd of Topology mapping\n");
	}


	private void printVLUNConnections() {
		System.out.println("VLUN HOST CONNECTIONS:");
		d.getHostList().forEach(h -> {
			try {
				System.out.println(h.getName() + " has " + h.getVolumeList().size()+ " volumes connected." );
				System.out.println(h.getName() + " has " + h.getVolumesetList().size()+ " connected.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		System.out.println("VLUN HOSTSET CONNECTIONS:");
		d.getHostsetList().forEach(hs -> {
			try {
				System.out.println(hs.getName() + " has " + hs.getVolumeList().size() + " volume connected.");
				System.out.println(hs.getName() + " has "  + hs.getVolumesetList().size() + " volumeset connected.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		System.out.println("VLUN VOLUME CONNECTIONS:");
		d.getVolumeList().forEach(h -> {
			try {
				System.out.println(h.getName() + " has " + h.getHostList().size() + " hosts connected.");
				System.out.println(h.getName() + " has "  + h.getHostsetList().size() + " hostset connected.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		System.out.println("VLUN VOLUMESET CONNECTIONS:");
		d.getVolumesetList().forEach(vs -> {
			try {
				System.out.println(vs.getName() + " has " + vs.getHostList().size() + " hosts connected.");
				System.out.println(vs.getName() + " has "  + vs.getHostsetList().size() + " hostset connected.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}


	private void joinVolumesAndVolumesetsAndHostsAndHostsetsUsingVLUN() {
		System.out.println("\nVOLUMES, VOLUMESETS, HOSTS AND HOSTSETS");
		var hostVolumeCombo = new HashSet<String>();
		d.getVlunList().forEach(vlun -> {
			System.out.println("TESTING VLUN ID: " + vlun.getLun());
			String volumeName = vlun.getVolumeName();
			String hostName = vlun.getHostname();
			String thisCombo = hostName + ":" +volumeName; 
			
			if(!hostVolumeCombo.contains(thisCombo)) {
				
				// 1-1 volumeset and hostset
				subJoinVolumesetAndHostset(volumeName, hostName);
				
				// 0-0 volume and host
				subJoinVolumeAndHost(volumeName, hostName);
				
				// 1-0 volumeset and host
				subJoinVolumesetAndHost(volumeName, hostName);
				
				//0-1 volume and hostset
				subJoinVolumeAndHostset(volumeName, hostName);
				
				hostVolumeCombo.add(hostName + ":" +volumeName); 
			}
		// clear duplicates from 4 LUN entries	
		});
		/*
		 * d.getHostList().forEach( h-> { System.out.println(h.getName() + " 1"); try {
		 * Set<String> volumeSet = h.getVolumeList().stream().map(v ->
		 * v.getName()).collect(Collectors.toSet());
		 * 
		 * h.getVolumeList().clear(); System.out.println(h.getName() + " 2");
		 * volumeSet.forEach( str -> { h.getVolumeList().add(
		 * d.getVolumeCollection().getByName(str)); }); } catch (Exception e) {
		 * 
		 * System.out.println(h.getName() + " exception"); }
		 * System.out.println(h.getName() + " 3"); });
		 * System.out.println("Uniq vol-host"); d.getVolumeList().forEach( v-> {
		 * System.out.println(v.getName() + " start"); try { Set<String> hostSet =
		 * v.getHostList().stream().map(h -> h.getName()).collect(Collectors.toSet());
		 * v.getHostList().clear(); System.out.println(v.getName() + " middle");
		 * hostSet.forEach( str -> { v.getHostList().add(
		 * d.getHostCollection().getByName(str)); }); } catch (Exception e) {
		 * System.out.println(v.getName() + " exception"); }
		 * System.out.println(v.getName() + " finish"); });
		 */
		
	}


	private void subJoinVolumesetAndHostset(String volumeName, String hostName) {
		if(volumeName.contains("set:") && hostName.contains("set:")) {
			System.out.println("VOLUMESET & HOSTSET");
			String volumesetName = volumeName.replace("set:", "");
			String hostsetName = hostName.replace("set:", "");
			
			Hostset hostset = d.getHostsetCollection().getByName(hostsetName);
			Volumeset volumeset = d.getVolumesetCollection().getByName(volumesetName);
			if(hostset != null && volumeset != null) {
				if(hostset.getVolumesetList()==null) {
					hostset.setVolumesetList(new ArrayList<Volumeset>());
				}
				hostset.getVolumesetList().add(volumeset);
				if(volumeset.getHostsetList()==null) {
					volumeset.setHostsetList(new ArrayList<Hostset>());
				}
				volumeset.getHostsetList().add(hostset);
				System.out.println("\tlinked: " + hostset.getName() + " and " + volumeset.getName());
			}
		}
	}


	private void subJoinVolumesetAndHost(String volumeName, String hostName) {
		if(volumeName.contains("set:") && !hostName.contains("set:")) {
			System.out.println("VOLUMESET & HOST");
			String volumesetName = volumeName.replace("set:", "");

			Host host = d.getHostCollection().getByName(hostName);
			Volumeset volumeset = d.getVolumesetCollection().getByName(volumesetName);
			
			if(host != null && volumeset != null) {
				if(host.getVolumesetList()==null) {
					host.setVolumesetList(new ArrayList<Volumeset>());
				}
				host.getVolumesetList().add(volumeset);
				if(volumeset.getHostList()==null) {
					volumeset.setHostList(new ArrayList<Host>());
				}
				volumeset.getHostList().add(host);
				System.out.println("\tlinked: host " + host.getName() +
						" and volumeset" + volumeset.getName());
			}
		}
	}


	private void subJoinVolumeAndHostset(String volumeName, String hostName) {
		if(!volumeName.contains("set:") && hostName.contains("set:")) {
			System.out.println("VOLUME & HOSTSET");
			String hostsetName = hostName.replace("set:", "");
			
			Volume volume = d.getVolumeCollection().getByName(volumeName);
			Hostset hostset = d.getHostsetCollection().getByName(hostsetName);
			if(volume != null && hostset != null) {
				if(volume.getHostsetList()==null) {
					volume.setHostsetList(new ArrayList<Hostset>());
				}
				volume.getHostsetList().add(hostset);
				if(hostset.getVolumeList()==null) {
					hostset.setVolumeList(new ArrayList<Volume>());
				}
				hostset.getVolumeList().add(volume);
				System.out.println("\tlinked: volume " + volume.getName() +
						" to hostset: " + hostset.getName());
			}
		}
	}


	private void subJoinVolumeAndHost(String volumeName, String hostName) {
		if(!volumeName.contains("set:") && !hostName.contains("set:")) {
			System.out.println("VOLUME & HOST");
//			System.out.println("\ttesting with [" + volumeName + "], and [" + hostName + "]");
			Volume volume = d.getVolumeCollection().getByName(volumeName);
			Host host = d.getHostCollection().getByName(hostName);
			System.out.println("\tFound: " + volume.getName());
			System.out.println("\tFound: " + host.getName());
			
			if(volume != null && host != null) {
				if(volume.getHostList()==null) {
					volume.setHostList(new ArrayList<Host>());
				}
				volume.getHostList().add(host);
				if(host.getVolumeList()==null) {
					host.setVolumeList(new ArrayList<Volume>());
				}
				host.getVolumeList().add(volume);
				System.out.println("\tlinked: " + host.getName() + " and " + volume.getName());
			} else {
				System.out.println("Either is null");
			}
		}
	}

	private void joinPhysicalDrivesAndDriveEnclosures() {
		System.out.println("\nDRIVES AND ENCLOSURES");
		d.getPhysicalDriveList().forEach(drive -> {
			d.getDriveEnclosureList().forEach(enclosure -> {
				if(drive.getDriveEnclosureId()==enclosure.getId()) {
					drive.setDriveEnclosure(enclosure);
					if(enclosure.getPhysicalDriveList()==null) {
						enclosure.setPhysicalDriveList(new ArrayList<PhysicalDrive>());
					}
					enclosure.getPhysicalDriveList().add(drive);
					System.out.println("\tlinked drive:" + drive.getId() + " and " + enclosure.getName());
				}
			});
		});
		
	}

	private void joinPortsAndDriveEnclosures() {
		System.out.println("\nPORT AND DRIVEENCLOSURE");
		d.getDriveEnclosureList().forEach(enclosure -> {
			d.getPortList().forEach(port -> {
				if(enclosure.getAPortString().equals(port.getPortId())) {
					enclosure.setLoopAPort(port);
					if(port.getDeviceList()==null) {
						port.setDeviceList(new ArrayList<PortDevice>());
					}
					port.getDeviceList().add(enclosure);
					System.out.println("\tlinked LoopA " + port.getPortId() + " and " + enclosure.getName());
				}
				if(enclosure.getBPortString().equals(port.getPortId())) {
					enclosure.setLoopBPort(port);
					if(port.getDeviceList()==null) {
						port.setDeviceList(new ArrayList<PortDevice>());
					}
					port.getDeviceList().add(enclosure);
					System.out.println("\tlinked LoopB " + port.getPortId() + " and " + enclosure.getName());
				}				
			});
		});
		
	}

	private void joinVolumesetsAndVolumes() {
		System.out.println("\nVOLUME AND VOLUMESET");
		d.getVolumesetList().forEach(volumeset -> {
			System.out.println("\tChecking Volumeset: " + volumeset.getName());
			volumeset.getVolumeNameList().forEach(volName -> {
				System.out.println("\tChecking volumename: " + volName);				
				d.getVolumeList().forEach(volume -> {
					System.out.println("\tChecking volume: " + volume.getName());
					matchVolumeNameAndSetBidirectional(volumeset, volName, volume);
				});
			});
		});
	}

	private void matchVolumeNameAndSetBidirectional(Volumeset volumeset, String volName, Volume volume) {
		if(volume.getName().equals(volName)) {
			if(volumeset.getVolumeList()==null) {
				volumeset.setVolumeList(new ArrayList<Volume>());
			}
			volumeset.getVolumeList().add(volume);
			if(volume.getVolumesetList()==null) {
				volume.setVolumesetList(new ArrayList<Volumeset>());
			}
			volume.getVolumesetList().add(volumeset);
			System.out.println("\tlinked: " + volume.getName() + " and " + volumeset.getName());
		}
	}

	private void joinHostsAndHostsets() {
		System.out.println("\nHOST and HOSTSET");
		d.getHostList().forEach(host -> {
			System.out.println("\tChecking for host: " + host);
			d.getHostsetList().forEach(hostset -> {
				System.out.println("\tChecking for hostset: " + hostset);
				if(hostset.getHostNameList() != null) {
					matchHostNameAndSetBidirectional(host, hostset);
				} else {
					System.out.println("\tHostset hostlist is empty");
				}
			});
		});
	}

	private void matchHostNameAndSetBidirectional(Host host, Hostset hostset) {
		if(hostset.getHostNameList().contains(new String(host.getName()))) {
			host.setHostset(hostset);
			System.out.println("\tlinked: " + host.getName() + " to " + hostset.getName() );
			if(hostset.getHostList() == null) {
				hostset.setHostList(new ArrayList<Host>());
			}
			hostset.getHostList().add(host);
			System.out.println("\tlinked: " + hostset.getName() + " to " + host.getName());
		}
	}

	private void joinHostsetsAndStorage() {
		System.out.println("\nHOSTSET and STORAGE");
		d.getHostsetList().forEach(hostset -> {
			hostset.setSystem(d.getStorage());
			System.out.println("\tlinked: " + hostset.getName() + " to " + d.getStorage().getName());
		});
	}

	private void joinPortsAndHosts() {
		System.out.println("HOSTS & PORTS");
		getTypeOnePortList(d.getPortList())
			.forEach(port -> {
			System.out.println("Comparing for port: " + port.getPortId());
			for(Host host: d.getHostList()) {
				System.out.println("\tHOST: " + host.getName());
				// some host have empty FCPaths
				if(host.getFcPathList().size()>0) {
					setHostPortRelationship(port, host);
				}
			}
		});
	}

	private void joinControllersAndPorts() {
		System.out.println("CONTROLLERS & PORTS");
		d.getPortList().forEach(port -> {
			for (Node node : d.getControllerNodeList()) {
				if (node.getNodeId() == port.getPortPos().getNode()) {
					port.setNode(node);
					if(node.getPortList()==null) {
						node.setPortList(new ArrayList<Port>());
					}
					node.getPortList().add(port);
					System.out.println("\tlinked bidirectional: " + node.getName() + " to " + port.getPortId());
				}
			}
		});
	}

	private void joinStorageAndControllerNodes() {
		System.out.println("STORAGE & CONTROLLERS");
		d.getStorage().setControllerNodeList(d.getControllerNodeList());
		System.out.println("\tlinked: Storage to node");
		d.getControllerNodeList().forEach(node -> {
			node.setStorage(d.getStorage());
			System.out.println("\tlinked: " + node.getName() + " to " + d.getStorage().getName());
		});
	}

	private void setHostPortRelationship(Port port, Host host) {
		
		host.getFcPathList().forEach(fc -> {
			// some FCPath have no portPos object
			if (fc.getPortPos() != null) {
				PortPos pp = fc.getPortPos();
				String portId = getPortId(pp);
				matchPortIdAndSetBiRelationship(port, host, portId);
			}
		});
	}

	private void matchPortIdAndSetBiRelationship(Port port, Host host, String portId) {
		if (portId.equals(port.getPortId())) {
			if (port.getDeviceList() == null) {
				port.setDeviceList(new ArrayList<PortDevice>());
			}
			port.getDeviceList().add(host);
			System.out.println("\t\trelationship created: " + port.getPortId() + " to " + host.getName());
			if (host.getPortList() == null) {
				host.setPortList(new ArrayList<Port>());
			}
			host.getPortList().add(port);
			System.out.println("\t\trelationship created: " + host.getName() + " to " + port.getPortId());

		}
	}
	
	private List<Port> getTypeOnePortList(ArrayList<Port> portList) {
		return portList
		.stream()
		.filter(port -> (port.getType()==1))
		.collect(Collectors.toList());
	}
	
	private String getPortId(PortPos pp) {
		String id =pp.getNode() + ":" + pp.getSlot() + ":" + pp.getCardPort(); 
		System.out.println("\t\tcomparing id: " + id);
		return id;
	}
}