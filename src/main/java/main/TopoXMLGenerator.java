package main;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import hpe3par.base.DriveEnclosure;
import hpe3par.base.Host;
import hpe3par.base.Hostset;
import hpe3par.base.Node;
import hpe3par.base.PhysicalDrive;
import hpe3par.base.Port;
import hpe3par.base.PortDevice;
import hpe3par.base.StorageSystem;
import hpe3par.base.Volume;
import hpe3par.base.Volumeset;
import opcx.base.Instance;
import opcx.base.NewInstance;
import opcx.base.NewRelationship;
import opcx.base.Parent;
import opcx.base.Relations;
import opcx.base.Service;
import ucmdb.base.DriveEnclosureNI;
import ucmdb.base.HostNI;
import ucmdb.base.HostsetNI;
import ucmdb.base.NodeNI;
import ucmdb.base.PhysicalDriveNI;
import ucmdb.base.PortNI;
import ucmdb.base.StorageSystemNI;
import ucmdb.base.VolumeNI;
import ucmdb.base.VolumesetNI;

public class TopoXMLGenerator {
	
	private SystemDiscovery d = null;

	public TopoXMLGenerator(SystemDiscovery disco) {
		d = disco;
	}

	public String create() throws JAXBException {
		
		Service svc = new Service();
		JAXBContext jc = JAXBContext.newInstance(Service.class);

		ArrayList<NewInstance> newInstanceList = new ArrayList<>();
		ArrayList<NewRelationship> newRelationshipList = new ArrayList<>();
		
		/*
		 * Storage & Controller Node
		 */
		NewInstance ssInstance = getNewInstance(d.getStorage());
		newInstanceList.add(ssInstance);
		for(Node n: d.getControllerNodeList()) {
			NewInstance inst = getNewInstance(n);
			newInstanceList.add(inst);
		}

//		NewRelationship nRel = getStorageNodeRelationship(d.getStorage(), d.getControllerNodeList());
//		System.out.println("Storage to Node Relationship: " + nRel.toString());
//		newRelationshipList.add(nRel);
		
		//All Relationships to Storage
		newRelationshipList.add(getStorageDirectChildren());
		
		
		/*
		 * Controller & Port
		 */
		System.out.println("STARTING CONTROLLER AND PORT");
		d.getPortList().forEach(port -> {
			newInstanceList.add(getNewInstance(port));
		});
		newRelationshipList.addAll(getNodeAndPortRelationship());
		
		/*
		 * Host & DriveEnclosures, port relationship
		 */
		System.out.println("STARTING HOST DRIVEENCL PORT");
		d.getDriveEnclosureList().forEach(encl -> {
			newInstanceList.add(getNewInstance(encl));
		});
		d.getHostList().forEach(host -> {
			newInstanceList.add(getNewInstance(host));
		});
		newRelationshipList.addAll(getPortAndDeviceRelationship());
		
		/*
		 * Physical Drives and Relationship with Drive Enclosures
		 */ 
		System.out.println("STARTING DRIVE N DRIVEENCL Relationships");
		d.getPhysicalDriveList().forEach(drive -> {
			newInstanceList.add(getNewInstance(drive));
		});
		newRelationshipList.addAll(getDriveAndEnclosureRelationship());
		
		/*
		 * Hostset, Hostset and Host relationship
		 */
	    d.getHostsetList().forEach(hostset -> {
	    	newInstanceList.add(getNewInstance(hostset));
	    });
	    newRelationshipList.addAll(getHostsetAndHostRelationship());
	    
	    /*
	     * Volumeset and Volume Instance, Volumeset & Volume relationship
	     */
	    d.getVolumesetList().forEach(volumeset -> {
	    	newInstanceList.add(getNewInstance(volumeset));
	    });
	    d.getVolumeList().forEach(volume -> {
	    	newInstanceList.add(getNewInstance(volume));
	    });
	    newRelationshipList.addAll(getVolumesetRelationships());
	    
	    /* 
	     *  Host and Volume Relationship
	     */
	    newRelationshipList.addAll(getHostAndVolumeRelationships());
	    
		/*
		 * Not to be modified below
		 */
		svc.setNewInstanceList(newInstanceList);
		svc.setNewRelationshipList(newRelationshipList);
		
	    Marshaller marshaller = jc.createMarshaller();
	    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    marshaller.marshal(svc, baos);
	    String xml = null;
	    xml = new String(baos.toByteArray());
	    System.out.println("XML Length: " + xml.length());
		return xml;
		
	}
	
	private Collection<? extends NewRelationship> getHostAndVolumeRelationships() {
		String type = "dependency";
		System.out.println("Inside getHostAndVolumeRelationships");
		ArrayList<NewRelationship> newRelList = new ArrayList<NewRelationship>(); 
		
		d.getHostList().stream()
		.filter(h -> (h.getVolumeList()!=null))
		.forEach(host -> {
			System.out.println("For Host: " + host.getName());
			NewRelationship newRelationship = new NewRelationship();
			
			Parent parent = getParent(new HostNI(host).getRef());
			System.out.println("Parent set: " + parent.toString());
			newRelationship.setParent(parent);
			
			ArrayList<Relations> rList = new ArrayList<>();
			host.getVolumeList().forEach(volume -> {
				System.out.println("\tAttaching Volume: " + volume.getName());
				Relations r= getRelations(type, new VolumeNI(volume).getRef());
				rList.add(r);
				
			});
			newRelationship.setRelationsList(rList);
			newRelList.add(newRelationship);
		});
		System.out.println("HOST-VOLUME: " + newRelList);
		return newRelList;
	}

	private NewRelationship getStorageDirectChildren() {
		// TODO Auto-generated method stub
		
		System.out.println("Inside getStorageDirectChildren");
		
		NewRelationship newRelationship = new NewRelationship();
		ArrayList<Relations> relationsList = new ArrayList<>();
		
		Parent parent = getParent(new StorageSystemNI(d.getStorage()).getRef());
		newRelationship.setParent(parent);
		System.out.println("Parent set: " + parent.toString());
		
		relationsList.addAll(getControllerChildren());
		relationsList.addAll(getVolumesChildren());
		relationsList.addAll(getHostsetChildren());
		if(d.getVolumesetList()!=null && d.getVolumesetList().size()>0) {
			relationsList.addAll(getVolumesetChildren());
		}
			
		newRelationship.setRelationsList(relationsList);
		
		return newRelationship;
	}

	private ArrayList<Relations> getVolumesetChildren() {
		ArrayList<Relations> relationsList = new ArrayList<>();
		String type = "composition";
		for(Volumeset o: d.getVolumesetList()) {
			Relations relation = getRelations(type, new VolumesetNI(o).getRef());
			relationsList.add(relation);
		}
		return relationsList;
	}

	private ArrayList<Relations> getHostsetChildren() {
		ArrayList<Relations> relationsList = new ArrayList<>();
		String type = "composition";
		for(Hostset o: d.getHostsetList()) {
			Relations relation = getRelations(type, new HostsetNI(o).getRef());
			relationsList.add(relation);
		}
		return relationsList;
	}

	private ArrayList<Relations> getVolumesChildren() {
		ArrayList<Relations> relationsList = new ArrayList<>();
		String type = "composition";
		for(Volume o: d.getVolumeList()) {
			Relations relation = getRelations(type, new VolumeNI(o).getRef());
			relationsList.add(relation);
		}
		return relationsList;
	}

	private ArrayList<Relations> getControllerChildren() {
		ArrayList<Relations> relationsList = new ArrayList<>();
		String type = "membership";
		for(Node n: d.getControllerNodeList()) {
			Relations relation = getRelations(type, new NodeNI(n).getRef());
			relationsList.add(relation);
		}
		return relationsList;
	}

	private ArrayList<NewRelationship> getVolumesetRelationships() {
		String type = "containment";
		System.out.println("Inside getVolumesetRelationships");
		ArrayList<NewRelationship> newRelList = new ArrayList<NewRelationship>(); 
		
		// some storage dont have VolumeSet
		if(d.getVolumesetList()==null || d.getVolumesetList().size()==0) {
			System.out.println("Volumeset list size is 0");
			return newRelList;
		}
		
		// STORAGE RELATION
//		try {
//			newRelList.add(getStorageAndVolumesetRelationship());
//		} catch(Exception e) {
//			System.out.println("getStorageAndVolumesetRelationship function");
//			e.printStackTrace();
//		}
		d.getVolumesetList().forEach(vSet -> {
			System.out.println("For Volumeset: " + vSet.getName());
			NewRelationship newRelationship = new NewRelationship();
			
			Parent parent = getParent(new VolumesetNI(vSet).getRef());
			System.out.println("Parent set: " + parent.toString());
			newRelationship.setParent(parent);
			
			ArrayList<Relations> rList = new ArrayList<>();
			vSet.getVolumeList().forEach(volume -> {
				System.out.println("\tAttaching Volume: " + volume.getName());
				Relations r= getRelations(type, new VolumeNI(d.getStorage(), volume).getRef());
				rList.add(r);
				
			});
			newRelationship.setRelationsList(rList);
			newRelList.add(newRelationship);
		});
		System.out.println(newRelList);
		return newRelList;
	}

	/*
	 * private NewRelationship getStorageAndVolumesetRelationship() { String type =
	 * "composition";
	 * System.out.println("Inside getStorageAndVolumesetRelationship");
	 * 
	 * NewRelationship newRelationship= new NewRelationship(); Parent parent =
	 * getParent(new StorageSystemNI(d.getStorage()).getRef());
	 * newRelationship.setParent(parent);
	 * 
	 * ArrayList<Relations> rList = new ArrayList<>();
	 * d.getVolumesetList().forEach(volumeset -> {
	 * System.out.println("\tAttaching Volumeset: " + volumeset.getName());
	 * Relations r= getRelations(type, new VolumesetNI(volumeset).getRef());
	 * rList.add(r); }); newRelationship.setRelationsList(rList);
	 * System.out.println(newRelationship); return newRelationship; }
	 * 
	 * private NewRelationship getStorageAndHostsetRelationship() { String type =
	 * "composition"; System.out.println("Inside getStorageAndHostsetRelationship");
	 * 
	 * NewRelationship newRelationship= new NewRelationship(); Parent parent =
	 * getParent(new StorageSystemNI(d.getStorage()).getRef());
	 * newRelationship.setParent(parent);
	 * 
	 * ArrayList<Relations> rList = new ArrayList<>();
	 * d.getHostsetList().forEach(hostset -> {
	 * System.out.println("\tAttaching Hostset: " + hostset.getName()); Relations r=
	 * getRelations(type, new HostsetNI(hostset).getRef()); rList.add(r); });
	 * newRelationship.setRelationsList(rList); System.out.println(newRelationship);
	 * return newRelationship;
	 * 
	 * }
	 */

	private ArrayList<NewRelationship> getHostsetAndHostRelationship() {
		String type = "membership";
		System.out.println("Inside getHostsetAndHostRelationship");
		ArrayList<NewRelationship> newRelList = new ArrayList<NewRelationship>(); 
		
		// STORAGE RELATION
		//TO DO removal
//		try {
//			newRelList.add(getStorageAndHostsetRelationship());
//		} catch(Exception e) {
//			System.out.println("getStorageAndHostsetRelationship function");
//			e.printStackTrace();
//		}
		
		d.getHostsetList().forEach(hostset -> {
			System.out.println("For Hostset: " + hostset.getName());
			if(hostset.getHostList() != null) {
				NewRelationship newRelationship = new NewRelationship();
				
				Parent parent = getParent(new HostsetNI(hostset).getRef());
				System.out.println("Parent set: " + parent.toString());
				newRelationship.setParent(parent);
				
				ArrayList<Relations> rList = new ArrayList<>();
				hostset.getHostList().forEach(host -> {
					System.out.println("\tAttaching Host: " + host.getName());
					Relations r= getRelations(type, new HostNI(host).getRef());
					rList.add(r);
					
				});
				newRelationship.setRelationsList(rList);
				newRelList.add(newRelationship);
			} else {
				System.out.println("Host list empty.");
			}
		});
		System.out.println(newRelList);
		return newRelList;
	}

	private ArrayList<NewRelationship> getDriveAndEnclosureRelationship() {
		String type = "composition";
		System.out.println("Inside getDriveAndEnclosureRelationship");
		ArrayList<NewRelationship> newRelList = new ArrayList<NewRelationship>(); 
		d.getDriveEnclosureList().forEach(encl -> {
			System.out.println("For Enclosure: " + encl.getName());
			NewRelationship newRelationship = new NewRelationship();
			
			Parent parent = getParent(new DriveEnclosureNI(encl).getRef());
			System.out.println("Parent set: " + parent.toString());
			newRelationship.setParent(parent);
			
			ArrayList<Relations> rList = new ArrayList<>();
			encl.getPhysicalDriveList().forEach(drive -> {
				System.out.println("\tAttaching PhysicalDrive: " + drive.getId());
				Relations r= getRelations(type, new PhysicalDriveNI(drive).getRef());
				rList.add(r);
				
			});
			newRelationship.setRelationsList(rList);
			newRelList.add(newRelationship);
		});
		System.out.println(newRelList);
		return newRelList;

	}

	private ArrayList<NewRelationship> getPortAndDeviceRelationship() {
		System.out.println("Inside getPortAndDeviceRelationship");
		String type = "consumer_provider";
		ArrayList<NewRelationship> newRelList = new ArrayList<NewRelationship>();
		d.getPortList().stream()
		.filter(port -> (port.getType()==1 || port.getType()==2))
		.forEach(port -> {
			System.out.println("For port: " + port.getPortId());
			NewRelationship newRelationship = new NewRelationship();
			
			Parent parent = getParent(new PortNI(port).getRef());
			System.out.println("Parent set: " + parent.toString());
			newRelationship.setParent(parent);
			
			ArrayList<Relations> rList = new ArrayList<>();
			
			for(PortDevice d: port.getDeviceList()) {
				System.out.println("\tFor PortDevice: " + d.getName());
				// HOST type
				if(port.getType()==1) {
					Relations r= getRelations(type, new HostNI(d).getRef());
					rList.add(r);
				}
				// CAGE type
				if(port.getType()==2) {
					Relations r= getRelations(type, new DriveEnclosureNI(d).getRef());
					rList.add(r);
				}
			}
			newRelationship.setRelationsList(rList);
			newRelList.add(newRelationship);
		});
		
		System.out.println(newRelList);
		return newRelList;
	}

	public ArrayList<NewRelationship> getNodeAndPortRelationship() {
		String type = "composition";
		ArrayList<NewRelationship> newRelList = new ArrayList<NewRelationship>(); 
		d.getControllerNodeList().forEach(node -> {
			System.out.println("For node: " + node.getName());
			NewRelationship newRelationship = new NewRelationship();
			
			Parent parent = getParent(new NodeNI(node).getRef());
			System.out.println("Parent set: " + parent.toString());
			newRelationship.setParent(parent);
			ArrayList<Relations> rList = new ArrayList<>();
			for(Port p: node.getPortList()) {
				System.out.println("\tFor port: " + p.getPortId());
				Relations r= getRelations(type, new PortNI(p).getRef());
				rList.add(r);
			}
			newRelationship.setRelationsList(rList);
			newRelList.add(newRelationship);
		});
		System.out.println(newRelList);
		return newRelList;
		
	}

	public NewRelationship getStorageNodeRelationship(StorageSystem s, ArrayList<Node> controllerNodeList) {
		System.out.println("Inside getStorageNodeRelationship");
		String type = "membership";
		
		NewRelationship nRel = new NewRelationship();
		
		Parent parent = getParent(new StorageSystemNI(s).getRef());
		nRel.setParent(parent);
//		System.out.println("Parent set: " + parent.toString());
		ArrayList<Relations> rList = new ArrayList<>();
		for(Node n: controllerNodeList) {
//			System.out.println("Starting for: " + n.getName());
			Relations r= getRelations(type, new NodeNI(n).getRef());
			rList.add(r);
		}
		
		nRel.setRelationsList(rList);
		
		return nRel;
	}

	public NewInstance getNewInstance(Object obj) {
//		System.out.println("inside getInstance");
		NewInstance newInstance = null;
		if(obj instanceof StorageSystem ) {
			newInstance = new StorageSystemNI(obj); 
		} 
		if(obj instanceof Node) {
			newInstance = new NodeNI(obj);
		}
		if (obj instanceof Port) {
			newInstance = new PortNI(obj);
		}
		if (obj instanceof DriveEnclosure) {
			newInstance = new DriveEnclosureNI(obj);
		}
		if (obj instanceof Host) {
			newInstance = new HostNI(obj);
		}
		if (obj instanceof PhysicalDrive) {
			newInstance = new PhysicalDriveNI(obj);
		}
		if (obj instanceof Hostset) {
			newInstance = new HostsetNI(obj);
		}
		if (obj instanceof Volumeset) {
			newInstance = new VolumesetNI(obj);
		}
		if (obj instanceof Volume) {
			newInstance = new VolumeNI(d.getStorage(), obj);
		}
		return newInstance;
	}
	
	private Relations getRelations(String type, String ref) {
		Relations r = new Relations();
		r.setInstance(getInstance(ref ));
		r.setType(type);
		return r;
	}
	
	private Instance getInstance(String ref) {
//		System.out.println("insdie getInstance with ref " + ref);
		Instance i = new Instance();
		i.setRef(ref);
		return i;
	}


	private Parent getParent(String ref) {
//		System.out.println("Inside getParent for ref " + ref);
		Parent p = new Parent();
		p.setInstance(getInstance(ref));
		return p;
	}

	//change to private after testing.
//	private NewInstance getStorageSystemInstance(StorageSystem storage) {
//		 
//		@SuppressWarnings({ "rawtypes", "unchecked" })
//		List<Attribute> attribList = new ArrayList(
//				Arrays.asList(
//						new Attribute("hpom_discoTarget", "BSM"),
//						new Attribute("hpom_citype", "node"),
//						new Attribute("primary_ip_address", storage.getIPv4Addr()), 
//						new Attribute("ucmdb_name", storage.getName())
//				));
//		 System.out.println("Storage: " + storage.toString());
//		 NewInstance inst = new NewInstance();
//		 inst.setKey(storage.getName() + ":" + storage.getId());
//		 inst.setRef(storage.getName() + ":" + storage.getId()); 
//		 inst.setStd("DiscoveredElement"); 
//		 inst.setAttribute(attribList);
//		return inst;
//	}

}
