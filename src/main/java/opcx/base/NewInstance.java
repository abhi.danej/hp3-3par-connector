package opcx.base;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NewInstance")
public class NewInstance {

	@XmlElement
	protected String Key;

	@XmlAttribute
	protected String ref;
	
	@XmlElement
	protected String Std = "DiscoveredElement";

	@XmlElementWrapper(name="Attributes")
	protected List<Attribute> Attribute;
}
