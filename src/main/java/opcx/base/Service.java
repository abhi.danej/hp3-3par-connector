package opcx.base;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name="Service")
@XmlAccessorType(XmlAccessType.FIELD)
public class Service {

	@XmlElement(name="NewInstance")
	private ArrayList<NewInstance> newInstanceList;
	
	@XmlElement(name="NewRelationship")
	private ArrayList<NewRelationship> newRelationshipList;
}
