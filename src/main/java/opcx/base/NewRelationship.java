package opcx.base;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NewInstance")
public class NewRelationship {

	@XmlElement(name="Parent")
	public Parent parent;
	
	@XmlElementWrapper(name="GenericRelations")
	@XmlElement(name="Relations")
	public ArrayList<Relations> relationsList;
}
