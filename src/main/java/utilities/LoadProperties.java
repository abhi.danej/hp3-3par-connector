package utilities;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import main.SystemDiscovery;

public class LoadProperties {

	public Properties props = new Properties();
	
	private static LoadProperties lp = null;
	
	public static LoadProperties getInstance() throws FileNotFoundException, IOException {
		if(lp==null) {
			lp = new LoadProperties();
		}
		return lp;
	}
	
	private LoadProperties() throws FileNotFoundException, IOException {
		System.out.println("Loading properties..");
//		props.load(new FileReader("conf\\conf.properties"));

		props.load(new FileReader(SystemDiscovery.configFilePath));
	}
}
