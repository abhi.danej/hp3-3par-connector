package utilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectConverter<OutputType> {
	
	@SuppressWarnings("rawtypes")
	static ObjectConverter oc;
	
	public static ObjectConverter getInstance() {
		if(oc==null) {
			oc = new ObjectConverter();
		}
		return oc;
	}
	
	public OutputType getObject2(
				String jsonText, 
				final Class<OutputType> outputType) 
			throws JsonMappingException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		OutputType type = mapper.readValue(jsonText, outputType);
		return type;
	}

//	public OutputType getObject(String jsonText, final Class<OutputType> outputType) throws JsonMappingException, JsonProcessingException {
//		
//		ObjectMapper om = new ObjectMapper();
//		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		
//		OutputType type = om.readValue(jsonText, outputType);
//		
//		return type;
//		
//	}
	
}
