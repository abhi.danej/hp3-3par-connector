package utilities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.Service;

public class PojoToXmlConverter {
	
	public void convert() throws JAXBException {
		
    JAXBContext jc = JAXBContext.newInstance(NewInstance.class);

    List<Attribute> aList = new ArrayList();
    Attribute a1 = new Attribute();
    a1.setName("ucmdb_name");
    a1.setValue("business_application");
    aList.add(a1);
    
    Attribute a2 = new Attribute();
    a2.setName("ucmdb_monitored_by");
    a2.setValue("BSMC:Opscx");
    aList.add(a2);
    
    NewInstance n1 = new NewInstance();
    n1.setKey("3par 8200");
    n1.setRef("3par");
    n1.setStd("DiscoveredElement");
    n1.setAttribute(aList);
    
    Marshaller marshaller = jc.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    marshaller.marshal(n1, System.out);
	}
}
