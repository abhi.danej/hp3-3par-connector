package utilities;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TopoWriter {
	
	private Path filePath, dirPath;
	private static TopoWriter fw = null;
	
	public static TopoWriter getInstance() throws IOException {
		if(fw ==null) {
			fw = new TopoWriter();
		}
		return fw;
	}
	
	private TopoWriter() throws IOException {
		
		dirPath = Paths.get("log");
		filePath = Paths.get("log", getFileName());		

		createIfNeeded();
	}

	private void createIfNeeded() throws IOException {
		if(Files.notExists(dirPath)) {
			Files.createDirectory(Paths.get("log"));
		}
		
		if(Files.notExists(filePath)) {
			Files.createFile(filePath);
		}
	}

	public void append(String inputString) throws IOException {

//			Write content to file
		Files.writeString(filePath,inputString);
//			System.out.println(content);

	}

	private String getFileName() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd-HH-mm-ss");
		StringBuilder sb = new StringBuilder("topology-");
		sb.append(formatter.format(new Date()));
		sb.append(".txt");
		return sb.toString();
	}

}
