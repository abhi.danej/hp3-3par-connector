package utilities;

import java.util.ArrayList;
import java.util.List;

import javax.management.relation.Relation;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import opcx.base.Attribute;
import opcx.base.Instance;
import opcx.base.NewInstance;
import opcx.base.NewRelationship;
import opcx.base.Parent;
import opcx.base.Relations;
import opcx.base.Service;

public class PojoToXmlConverter2 {
	
	public void convert() throws JAXBException {
		
	Service svc = new Service();
		
    JAXBContext jc = JAXBContext.newInstance(Service.class);

    List<Attribute> aList = new ArrayList();
    Attribute a1 = new Attribute();
    a1.setName("ucmdb_name");
    a1.setValue("business_application");
    aList.add(a1);
    
    Attribute a2 = new Attribute();
    a2.setName("ucmdb_monitored_by");
    a2.setValue("BSMC:Opscx");
    aList.add(a2);
    
    NewInstance n1 = new NewInstance();
    n1.setKey("3par 8200");
    n1.setRef("3par");
    n1.setStd("DiscoveredElement");
    n1.setAttribute(aList);
    
    NewRelationship rel1 = new NewRelationship();
    Instance instance = new Instance();
    instance.setRef("VM:myUniq1");
    Instance instanceChild = new Instance();
    instanceChild.setRef("SQL:mysql");
    
    Parent p = new Parent();
    p.setInstance(instance);
    
    rel1.setParent(p);

    Relations relation1 = new Relations();
    relation1.setInstance(instanceChild);
    relation1.setType("containment");
    
    svc.setNewInstanceList(new ArrayList<NewInstance>());
    svc.getNewInstanceList().add(n1);
    
    rel1.setRelationsList(new ArrayList<Relations>());
    rel1.getRelationsList().add(relation1);
    
    svc.setNewRelationshipList(new ArrayList<NewRelationship>());
    svc.getNewRelationshipList().add(rel1);
    System.out.println("size of svc newinst array: " + svc.getNewInstanceList().size());
    
    
    Marshaller marshaller = jc.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    marshaller.marshal(svc, System.out);
	}
}
