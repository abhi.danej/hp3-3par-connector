package ucmdb.base;

import java.util.ArrayList;
import java.util.Arrays;

import hpe3par.base.StorageSystem;
import hpe3par.base.Volume;
import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.NewInstanceIf;

public class VolumeNI extends NewInstance implements NewInstanceIf{

	Volume o = null;
	StorageSystem storage = null;
			
	public VolumeNI(Object obj) {
		o = (Volume) obj;
//		System.out.println("inside constructor VolumeNI");
		setKey();
		setAttributeList();
		setRef();
	}
	public VolumeNI(StorageSystem storage, Object obj) {
		this.storage = storage;
		o = (Volume) obj;
		System.out.println("inside constructor2 VolumeNI");
		setKey();
		setAttributeList();
		setRef();
	}
	
	@Override
	public void setKey() {
		this.Key = o.getName()+ ":" + o.getId();
		this.ref = Key;
	}
	
	
	// root_container is mandatory for logical_volume class
	@Override
	public void setAttributeList() {
		this.Attribute = new ArrayList<Attribute>(
				Arrays.asList(
						new Attribute("hpom_discoTarget", "BSM"),
						new Attribute("hpom_citype", "logical_volume"),
//						new Attribute("ucmdb_root_container", storage.getName()),
						new Attribute("ucmdb_user_label", "Volume " + o.getName()),
						new Attribute("ucmdb_name", o.getName() )
						));
	}

	@Override
	public void setRef() {
//		ref = Key;
	}

}
