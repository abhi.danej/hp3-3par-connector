package ucmdb.base;

import java.util.ArrayList;
import java.util.Arrays;

import hpe3par.base.Hostset;
import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.NewInstanceIf;

public class HostsetNI extends NewInstance implements NewInstanceIf{

	Hostset o = null;
			
	public HostsetNI(Object obj) {
		o = (Hostset) obj;
//		System.out.println("inside constructor HostsetNI");
		setKey();
		setAttributeList();
		setRef();
	}
	
	@Override
	public void setKey() {
		this.Key = o.getName()+ ":" + o.getId();
		this.ref = Key;
	}
	
	@Override
	public void setAttributeList() {
		this.Attribute = new ArrayList<Attribute>(
				Arrays.asList(
						new Attribute("hpom_discoTarget", "BSM"),
						new Attribute("hpom_citype", "ci_collection"),
						new Attribute("ucmdb_ci_collection_id", this.getKey()),
						new Attribute("ucmdb_user_label","Hostset " + o.getName()),
						new Attribute("ucmdb_name", o.getName() )
						));
	}

	@Override
	public void setRef() {
//		ref = Key;
	}

}
