package ucmdb.base;

import java.util.ArrayList;
import java.util.Arrays;

import hpe3par.base.DriveEnclosure;
import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.NewInstanceIf;

public class DriveEnclosureNI extends NewInstance implements NewInstanceIf{

	DriveEnclosure o = null;
			
	public DriveEnclosureNI(Object obj) {
		o = (DriveEnclosure) obj;
		System.out.println("inside constructor DriveEnclosureNI");
		setKey();
		setAttributeList();
		setRef();
	}
	
	@Override
	public void setKey() {
		this.Key = o.getName()+ ":" + o.getId();
		this.ref = Key;
	}
	
	// name and port_index mandatory for physical_port cit
	@Override
	public void setAttributeList() {
		this.Attribute = new ArrayList<Attribute>(
				Arrays.asList(
						new Attribute("hpom_discoTarget", "BSM"),
						new Attribute("hpom_citype", "drive_enclosure"),
						new Attribute("ucmdb_name", o.getName() ),
						new Attribute("ucmdb_user_label", o.getName() + " " + o.getModel()),
						new Attribute("ucmdb_discovered_description", 
								"LoopA: " + o.getAPortString() +
								", LoopB: " + o.getBPortString())
						)
				);
	}

	@Override
	public void setRef() {
//		ref = Key;
	}

}
