package ucmdb.base;

import java.util.ArrayList;
import java.util.Arrays;

import hpe3par.base.PhysicalDrive;
import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.NewInstanceIf;

public class PhysicalDriveNI extends NewInstance implements NewInstanceIf{

	PhysicalDrive o = null;
			
	public PhysicalDriveNI(Object obj) {
		o = (PhysicalDrive) obj;
//		System.out.println("inside constructor PhysicalDriveNI");
		setKey();
		setAttributeList();
		setRef();
	}
	
	@Override
	public void setKey() {
		this.Key = "Drive" + ":" + o.getId();
		this.ref = Key;
	}
	
	// name and port_index mandatory for physical_port cit
	@Override
	public void setAttributeList() {
		this.Attribute = new ArrayList<Attribute>(
				Arrays.asList(
						new Attribute("hpom_discoTarget", "BSM"),
						new Attribute("hpom_citype", "disk_device"),
						new Attribute("ucmdb_name",
								"Drive " + o.getId() + " " + o.getType() + " " + o.getRpm() + "k" ),
						new Attribute("ucmdb_user_label", 
								"Drive " + o.getId() + " " + o.getType() + " (" + o.getRpm() + "k)" )
						));
	}

	@Override
	public void setRef() {
//		ref = Key;
	}

}
