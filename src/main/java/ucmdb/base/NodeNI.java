package ucmdb.base;

import java.util.ArrayList;
import java.util.Arrays;

import hpe3par.base.Node;
import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.NewInstanceIf;

public class NodeNI extends NewInstance implements NewInstanceIf {

	Node o = null;

	public NodeNI(Object obj) {
		o = (Node) obj;
//		System.out.println("inside constructor NodeNI");
		setRef();
		setKey();
		setAttributeList();
	}

	@Override
	public void setKey() {
		this.Key = o.getName() + ":" + o.getNodeId();
		this.ref = Key;
	}

	@Override
	public void setAttributeList() {
		this.Attribute = new ArrayList<Attribute>(
				Arrays.asList(
						new Attribute("hpom_discoTarget", "BSM"),
						new Attribute("hpom_citype", "storagearray"), 
						new Attribute("ucmdb_name", "Controller " + o.getName()),
						new Attribute("ucmdb_user_label", o.getName() + " Node " + o.getNodeId() ),
						new Attribute("ucmdb_discovered_description", 
								"id: " + o.getNodeId() +
								", isMaster: " + o.isMaster() +
								", inCluster: " + o.isInCluster()
								)
						)
				);
	}

	@Override
	public void setRef() {
//		ref = Key;
	}
}
