	package ucmdb.base;

import java.util.ArrayList;
import java.util.Arrays;

import hpe3par.base.Port;
import hpe3par.base.Port.PortType;
import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.NewInstanceIf;

public class PortNI extends NewInstance implements NewInstanceIf{

	Port o = null;
			
	public PortNI(Object obj) {
		o = (Port) obj;
//		System.out.println("inside constructor PortNI");
		setKey();
		setAttributeList();
		setRef();
	}
	
	@Override
	public void setKey() {
		this.Key = "Port:" + o.getPortId();
		this.ref = Key;
	}
	
	// name and port_index mandatory for physical_port cit
	@Override
	public void setAttributeList() {
		this.Attribute = new ArrayList<Attribute>(
				Arrays.asList(
						new Attribute("hpom_discoTarget", "BSM"),
						new Attribute("hpom_citype", "physical_port"),
						new Attribute("ucmdb_port_index", o.getPortId().replace(":", "")),
						new Attribute("ucmdb_name", 
							"Port " + o.getPortId() + " (" + PortType.toString(o.getType()) + ")" ),
						new Attribute("ucmdb_user_label", 
								"Port " + o.getPortId() + " " 
								+ Port.Protocol.toString(o.getProtocol())
								+ " (" + PortType.toString(o.getType()) + ")" 
								)
						));
	}

	@Override
	public void setRef() {
//		ref = Key;
	}

}
