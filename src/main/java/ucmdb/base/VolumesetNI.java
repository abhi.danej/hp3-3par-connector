package ucmdb.base;

import java.util.ArrayList;
import java.util.Arrays;

import hpe3par.base.Volumeset;
import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.NewInstanceIf;

public class VolumesetNI extends NewInstance implements NewInstanceIf{

	Volumeset o = null;
			
	public VolumesetNI(Object obj) {
		o = (Volumeset) obj;
//		System.out.println("inside constructor VolumesetNI");
		setKey();
		setAttributeList();
		setRef();
	}
	
	@Override
	public void setKey() {
		this.Key = o.getName()+ ":" + o.getId();
		this.ref = Key;
	}
	
	@Override
	public void setAttributeList() {
		this.Attribute = new ArrayList<Attribute>(
				Arrays.asList(
						new Attribute("hpom_discoTarget", "BSM"),
						new Attribute("hpom_citype", "volumegroup"),
						new Attribute("ucmdb_user_label", "Volumeset " + o.getName()),
						new Attribute("ucmdb_name", o.getName() )
						));
	}

	@Override
	public void setRef() {
//		ref = Key;
	}

}
