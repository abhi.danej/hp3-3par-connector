package ucmdb.base;

import java.util.ArrayList;
import java.util.Arrays;
import hpe3par.base.StorageSystem;
import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.NewInstanceIf;

//@Data
public class StorageSystemNI extends NewInstance implements NewInstanceIf{

	StorageSystem o = null;
			
	public StorageSystemNI(Object storage) {
		o = (StorageSystem) storage;
//		System.out.println("inside constructor StorageSystemNI");
		setRef();
		setKey();
		setAttributeList();
		
	}

	@Override
	public void setRef() {
//		ref = Key;
	}

	@Override
	public void setKey() {
		this.Key = o.getName() + ":" + o.getId();
		this.ref = Key;
	}

	@Override
	public void setAttributeList() {
		this.Attribute = new ArrayList<Attribute>(
				Arrays.asList(
						new Attribute("hpom_discoTarget", "BSM"),
						new Attribute("hpom_citype", "node"),
						new Attribute("ucmdb_name", o.getName()+ " System"),
						new Attribute("ucmdb_user_label", o.getName() + " Storage"),
						new Attribute("ucmdb_primary_ip_address", o.getIPv4Addr()), 
						new Attribute("ucmdb_discovered_description", 
								"id: " + o.getId() +
								", totalNodes: " + o.getTotalNodes() +
								", masterNode: " + o.getMasterNode() +
								", totalCapacityMiB: " + o.getTotalCapacityMiB() +
								", allocatedCapacityMiB: " + o.getAllocatedCapacityMiB() +
								", freeCapacityMiB: " + o.getFreeCapacityMiB()
								),
						new Attribute("ucmdb_discovered_model", o.getModel()),
						new Attribute("ucmdb_serial_number", o.getSerialNumber()),
						new Attribute("ucmdb_discovered_os_vendor", "HP"),
						new Attribute("ucmdb_discovered_os_name", "3PAR OS Suite"),
						new Attribute("ucmdb_discovered_os_version", o.getSystemVersion() + " " + o.getPatches())
				));
	}
}
