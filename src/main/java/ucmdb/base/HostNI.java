package ucmdb.base;

import java.util.ArrayList;
import java.util.Arrays;

import hpe3par.base.Host;
import opcx.base.Attribute;
import opcx.base.NewInstance;
import opcx.base.NewInstanceIf;

public class HostNI extends NewInstance implements NewInstanceIf{

	Host o = null;
			
	public HostNI(Object obj) {
		o = (Host) obj;
//		System.out.println("inside constructor HostNI");
		setKey();
		setAttributeList();
		setRef();
	}
	
	@Override
	public void setKey() {
		this.Key = o.getName()+ ":" + o.getId();
		this.ref = Key;
	}
	
	// name and port_index mandatory for physical_port cit
	@Override
	public void setAttributeList() {
		this.Attribute = new ArrayList<Attribute>(
				Arrays.asList(
						new Attribute("hpom_discoTarget", "BSM"),
						new Attribute("hpom_citype", getHostType()),
						new Attribute("ucmdb_name", o.getName())
						
						));
	}

	private String getHostType() {
		/*
		 * check if ESX or Windows machine, else set generic NODE type.
		 * This method can be extended for specific CI types.
		 * - Some host dont have 'descriptors' object, hence null condition 
		 */
		String ci_type = "host_node";
		if(o.getDescriptors() != null ) {			
			if(o.getDescriptors().getOs().toLowerCase().contains("esx")) {
				ci_type = "vmware_esx_server";
			}
			if (o.getDescriptors().getOs().toLowerCase().contains("window")) {
				ci_type = "nt";
			}
		}
		return ci_type;
	}

	@Override
	public void setRef() {
//		ref = Key;
	}

}
